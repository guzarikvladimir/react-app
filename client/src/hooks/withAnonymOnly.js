import React from 'react'
import { isAuthorized } from '../redux/modules/authorization/authActions'
import { useHistory } from 'react-router-dom'

export default (WrappedComponent) => {
  const history = useHistory()

  const withAnonymOnly = props => {
    if (!isAuthorized()) {
      return <WrappedComponent {...props} />
    }

    return history.goBack()
  }

  return withAnonymOnly
}