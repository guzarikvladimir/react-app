import React from 'react'
import { connect } from 'react-redux';
import { isAuthorized } from '../redux/modules/authorization/authActions';
import { Redirect } from 'react-router-dom';

export default (WrappedComponent) => {
  const WithAuthorization = props => {
    const { currentUser } = props
    
    if (!isAuthorized() && !currentUser) {
      const returnUrl = `${props.location.pathname}${props.location.search}`
      return <Redirect to={{
        pathname: '/login',
        search: `?returnUrl=${encodeURIComponent(returnUrl)}`}} 
      />
    } else {
      return <WrappedComponent {...props} />
    }
  }

  const mapStateToProps = (state) => ({
    currentUser: state.auth.currentUser
  })

  return connect(mapStateToProps)(WithAuthorization)
}