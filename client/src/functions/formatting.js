function normalizeValue(value) {
    if (value <= 0) {
        return ''
    }

    let rest = 1-Math.floor(Math.log(value)/Math.log(10))
    if (rest < 2) {
        rest = 2
    }

    return value.toFixed(rest).toString()
}

function dateToString(date) {
  const parsedDate = parseDate(date)
  const today = new Date()
  const yesterday = new Date(today.getFullYear(), today.getMonth(), today.getDate() - 1)
  const localTime = parsedDate.toLocaleTimeString()
  const localTimeShort = localTime.substring(0, localTime.lastIndexOf(':'))
  const localDate = parsedDate.toLocaleDateString()
  if (localDate === today.toLocaleDateString()) {
    return `Today, ${localTimeShort}`
  } else if (localDate === yesterday.toLocaleDateString()) {
    return `Yesterday, ${localTimeShort}`
  }

  return `${localDate}, ${localTimeShort}`
}

function dateToShortString(date) {
  const parsedDate = parseDate(date)
  const today = new Date()
  const yesterday = new Date(today.getFullYear(), today.getMonth(), today.getDate() - 1)
  const localDate = parsedDate.toLocaleDateString()
  if (localDate === today.toLocaleDateString()) {
    const localTime = parsedDate.toLocaleTimeString()
    return localTime.substring(0, localTime.lastIndexOf(':'))
  } else if (localDate === yesterday.toLocaleDateString()) {
    return `Yesterday`
  }

  return localDate
}

function dateToStatus(date) {
  const parsedDate = parseDate(date)
  const today = new Date()
  const diff = today-parsedDate;
  const diffMins = Math.round(diff / 60000)
  if (diffMins === 0) {
    return 'just yet'
  }
  if (diffMins < 60) {
    return `${diffMins} minutes ago`
  }

  return dateToString(date)
}

function parseDate(date) {
  const parts = date.split('T')

  return new Date(parts.join(' ') + ' UTC')
}

function toDataUrl(blob) {
  return new Promise((resolve, reject) => {
    const reader = new FileReader()
    reader.onloadend = () => resolve(reader.result)
    reader.onerror = reject
    reader.readAsDataURL(blob)
  })
}

export {
    normalizeValue,
    dateToString,
    dateToShortString,
    dateToStatus,
    toDataUrl
}