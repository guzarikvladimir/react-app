import React, { useEffect } from 'react'
import { BrowserRouter } from 'react-router-dom'
import { AlertState } from './context/alert/AlertState.js'
import { FirebaseState } from './context/firebase/FirebaseState.js'
import { ModalState } from './context/modal/ModalState.js'
import { CurrencyState } from './context/currency/CurrencyState.js'
import { Provider } from 'react-redux'
import { Routes } from './Routes.js'
import store from './redux/store.js'
import Navbar from './components/common/Navbar.js'
import { connectToChat } from './sockets/connection.js'
import { isAuthorized } from './redux/modules/authorization/authActions.js'

function App() {
  useEffect(() => {
    if (isAuthorized()) {
      connectToChat(store.dispatch)
    }
  }, [])

  return (
    <Provider store={store}>
      <FirebaseState>
        <AlertState>
          <ModalState>
            <CurrencyState>
              <BrowserRouter>
                <Navbar color="primary"/>
                <Routes />
              </BrowserRouter>
            </CurrencyState>
          </ModalState>
        </AlertState>
      </FirebaseState>
    </Provider>
  )
}

export default App
