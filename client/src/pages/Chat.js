import React, { useEffect } from 'react'
import { Container, makeStyles, Drawer } from '@material-ui/core'
import { Grid } from '@material-ui/core'
import { Paper } from '@material-ui/core'
import Contacts from '../components/chat/Contacts'
import Dialog from '../components/chat/Dialog'
import StatusHeader from '../components/chat/StatusHeader'
import { connect } from 'react-redux'
import { useState } from 'react'
import { fetchUserDialogs, getDialog, selectDialog } from '../redux/modules/chat/chatActions'
import { Loader } from '../components/common/Loader'
import Hidden from '@material-ui/core/Hidden'
import { Divider } from '@material-ui/core'
import { useParams } from 'react-router-dom'

const useStyles = makeStyles({
  fill: {
    height: 'calc(100vh - 90px)',
    width: '100%',
  },
  drawerPaper : {
    maxWidth: '300px'
  }
})

const Chat = (props) => {
  const classes = useStyles()
  const [mobileOpen, setMobileOpen] = useState(false);
  const { dialogs, dispatch } = props
  const { dialogId } = useParams()

  const handleDrawerToggle = () => {
    setMobileOpen(!mobileOpen);
  }

  const fetchSelectedDialog = dialogs => {
    let dialog = dialogs.find(d => d.id === dialogId)
    if (dialogId && dialog) {
      if (dialog.local) {
        dispatch(selectDialog(dialogId))
      } else {
        dispatch(getDialog(dialogId))
      }
    }
  }

  useEffect(() => {
    if (!dialogs || dialogs.length === 0) {
      dispatch(fetchUserDialogs()).then(dialogs => {
        fetchSelectedDialog(dialogs)
      })
    } else {
      fetchSelectedDialog(dialogs)
    }
    // eslint-disable-next-line
  }, [])

  return (
    <Container style={{padding:"0"}}>
      {!dialogs
      ? <Loader />
      : <Grid container className={classes.fill}>
          <Hidden xsDown>
            <Grid item md={3} sm={4}>
              <Paper square className={classes.fill}>
                <Contacts />
              </Paper>
            </Grid>
          </Hidden>
          <Hidden smUp>
            <Drawer
              variant="temporary"
              anchor={"left"}
              open={mobileOpen}
              classes={{paper: classes.drawerPaper}}
              onClose={handleDrawerToggle}
            >
              <Contacts />
            </Drawer>
          </Hidden>
          <Grid item md={9} sm={8} xs={12}>
            <Paper square className={classes.fill}>
              <StatusHeader mobileOpen={mobileOpen} handleDrawerToggle={handleDrawerToggle} />

              <Divider />

              <Dialog />
            </Paper>
          </Grid>
        </Grid>
      }
    </Container>
  )
}

const mapStateToProps = (state) => ({
  dialogs: state.chat.dialogs
})

const mapActionsToProps = (dispatch) => ({
  dispatch
})

export default connect(mapStateToProps, mapActionsToProps)(Chat)