import React, { useEffect } from 'react'
import { makeStyles, Modal, Backdrop, CircularProgress, IconButton, Button } from '@material-ui/core'
import { useParams, useHistory } from 'react-router-dom'
import { useState } from 'react'
import { connect } from 'react-redux'
import { getUserInfo } from '../redux/modules/chat/chatActions'
import PhotoCamera from '@material-ui/icons/PhotoCamera'
import { updateUserProfile } from '../redux/modules/userProfile/userProfileActions'
import { toDataUrl } from '../functions/formatting'
import { useContext } from 'react'
import { AlertContext } from '../context/alert/alertContext'

const useStyles = makeStyles(theme => ({
  container: {
    position: 'absolute',
    display: 'flex',
    alignItems: 'center',
  },
  boxContainer: {
    backgroundColor: 'RGB(255,255,255)',
    margin: 'auto',
    minWidth: '400px',
    padding: '10px'
  },
  header: {
    marginBottom: '10px',
    borderBottom: '1px solid RGB(222, 222, 222)'
  },
  photoContainer: {
    width: '160px',
    height: '160px',
    border: '1px solid RGB(222, 222, 222)',
    position: 'relative'
    /*display: 'flex',
    alignItems: 'center',
    justifyContent: 'center'*/
  },
  photoPreview: {
    position: 'absolute',
    left: '0',
    top: '0'
  },
  input: {
    display: 'none'
  },
  fullSize: {
    width: '100%',
    height: '100%',
  },
  body: {
    display: 'flex',
    paddingBottom: '10px',
    marginBottom: '10px',
    borderBottom: '1px solid RGB(222, 222, 222)'
  },
  info: {
    marginLeft: '10px'
  },
  footer: {
    textAlign: 'right'
  },
  button: {
    margin: '0px',
    marginLeft: '10px'
  },
  backdrop: {
    zIndex: '1'
  }
}))

const UserProfile = props => {
  const classes = useStyles()
  const history = useHistory()
  const { userId } = useParams()
  const [user, setUser] = useState(null)
  const [loading, setLoading] = useState(false)
  const [editing, setEditing] = useState(false)
  const { show } = useContext(AlertContext)

  function handleFileLoaded(event) {
    if (event.target.files[0]) {
      toDataUrl(event.target.files[0]).then(result => {
        setUser({ ...user, profileImg: result })
      })
    }
  }

  function handleClose() {
    history.push(props.locations?.state.returnUrl ?? "/")
  }

  function handleEdit() {
    setEditing(true)
  }

  function handleSave() {
    updateUserProfile(user).then(() => {
      show('The changes were saved', 'success')
      setEditing(false)
    })
    .catch(() => {
      show('An error occurred while saving', 'error')
    })
  }

  useEffect(() => {
    setLoading(true)
    getUserInfo(userId).then(data => {
      setUser(data)
    }).finally(() => {
      setLoading(false)
    })
    // eslint-disable-next-line
  }, [])

  return (
    <>
      {user
      ? <Modal
          open={true}
          onClose={handleClose}
          disableBackdropClick
          className={classes.container}
        >
          <div className={classes.boxContainer}>
            <div className={classes.header}>
              <h4>{user.name}</h4>
            </div>
            <div className={classes.body}>
              <div className={classes.photoContainer}>
                <img src={user.profileImg} alt="" className={classes.photoPreview + " " + classes.fullSize} />
                {editing
                ? <>
                    <input onChange={handleFileLoaded} className={classes.input} accept="image/*" id="icon-button-file" type="file"/>
                    <label className={classes.fullSize} htmlFor="icon-button-file">
                      <IconButton className={classes.fullSize} color="primary" aria-label="upload picture" component="span">
                        <PhotoCamera />
                      </IconButton>
                    </label>
                  </>
                : <></>}
              </div>
              <div className={classes.info}>
                <div>Email: {user.email}</div>
                {user.phoneNumber
                ? <div>Phone number: {user.phoneNumber}</div>
                : <></>}
              </div>
            </div>
            <div className={classes.footer}>
              {editing
              ? <Button variant="contained" size="small" color="primary" onClick={handleSave} className={classes.button}>
                  Save
                </Button>
              : <Button variant="contained" size="small" color="secondary" onClick={handleEdit} className={classes.button}>
                  Edit
                </Button>
              }
              <Button variant="contained" size="small" color="default" onClick={handleClose} className={classes.button}>
                  Close
              </Button>
            </div>
          </div>
        </Modal>
      : <Backdrop open={loading} className={classes.backdrop}>
          <CircularProgress color="inherit"/>
        </Backdrop>
      }
    </>
  )
}

export default connect()(UserProfile)