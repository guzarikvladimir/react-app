import React, { useState } from 'react'
import classNames from "classnames"
import { connect } from 'react-redux'
import { useHistory } from 'react-router-dom'
import { makeStyles } from '@material-ui/core'
import TextField from '@material-ui/core/TextField'
import Button from '@material-ui/core/Button'
import InputAdornment from '@material-ui/core/InputAdornment'
import IconButton from '@material-ui/core/IconButton'

import Visibility from '@material-ui/icons/Visibility'
import VisibilityOff from '@material-ui/icons/VisibilityOff'

import styles from '../content/styles/authorization/authorizationStyles.js'
import { register } from '../redux/modules/authorization/authActions.js'
import InputMask from 'react-input-mask'

const useStyles = makeStyles(styles)

const errors = { }
const Register = (props) => {
  const [submitting, setSubmitting] = useState(false)
  const [registered, setRegistered] = useState(false)
  const [model, setModel] = useState({
    email: { value: "", isValid: true },
    name: { value: "", isValid: true },
    phoneNumber: { value: "", isValid: true },
    password: { value: "", isValid: true, show: false },
    confirmPassword: { value: "", isValid: true, show: false },
  })

  const classes = useStyles()
  const history = useHistory()
  const { returnUrl, dispatch } = props

  const submitHandler = (event) => {
    event.preventDefault()
    if (!isValid()) {
      return
    }

    const registerModel = Object.getOwnPropertyNames(model).reduce((result, key) => {
      result[key] = model[key].value
      return result
    }, {})

    setSubmitting(true)
    dispatch(register(registerModel)).then(() => {
      // TODO: confirm window
      setRegistered(true)
    }).catch(({response}) => {
      try {
        const serverErrors = response.data.errors ?? response.data
        Object.getOwnPropertyNames(serverErrors).forEach(errKey=> {
          const lowerKey = errKey[0].toLowerCase() + errKey.slice(1)
          model[lowerKey].isValid = false
          errors[lowerKey] = serverErrors[errKey]
        })
  
        setSubmitting(false)
        setModel({ ...model })
      } catch { }
    })
  }

  const isValid = () => {
    let valid = true
    Object.getOwnPropertyNames(model).forEach((key) => {
      if (!model[key].value || model[key].value.trim() === "") {
        model[key].isValid = false
        valid = false
        errors[key] = "Field can not be empty"
      }
      if (key === "confirmPassword" && model.confirmPassword.value !== model.password.value) {
        model[key].isValid = false
        valid = false
        errors[key] = "Passwords do not match"
      }
    })

    if (valid) {
      return true
    }

    setModel({ ...model })
    return false
  }

  const setValue = (type, value) => {
    model[type].value = value
    model[type].isValid = true
    errors[type] = null

    setModel({ ...model })
  }

  const loginHandle = () => {
    const redirect = getReturnUrl()
    if (redirect) {
      history.replace(`/login?returnUrl=${encodeURIComponent(redirect)}`)
    } else {
      history.replace('/login')
    }
  }

  const getReturnUrl = () => {
    const redirect = returnUrl ?? new URLSearchParams(window.location.search).get('returnUrl')
    if (!redirect || redirect.indexOf('login') > 0) {
        return null
    }

    return redirect
  }

  const handleClickShowPassword = () => {
    model.password.show = !model.password.show

    setModel({ ...model });
  }

  const handleClickShowConfirmPassword = () => {
    model.confirmPassword.show = !model.confirmPassword.show

    setModel({ ...model });
  }

  const formContainerClass = classNames({
    [classes.formContainer]: true,
    [classes.registeredFormContainer]: true
  })

  return (
    <div className={classes.container}>
      <div className={classes.headerContainer}>
        <h4>Registration</h4>
        <p className={classes.infoText}>You need to register to join the chat</p>
      </div>
      <div className={formContainerClass}>
        {!registered
        ? <form onSubmit={submitHandler} className={classes.form}>
            <TextField
              placeholder="Email"
              value={model.email.value}
              onChange={(event) => setValue("email", event.target.value)}
              fullWidth
              variant="outlined"
              className={classes.input}
              error={model.email.isValid ? false : true}
              helperText={model.email.isValid ? "" : errors["email"]}
            />
            <TextField
              placeholder="Full name"
              value={model.name.value}
              onChange={(event) => setValue("name", event.target.value)}
              fullWidth
              variant="outlined"
              className={classes.input}
              error={model.name.isValid ? false : true}
              helperText={model.name.isValid ? "" : errors["name"]}
            />
            <InputMask 
              mask="+375 (99) 999-99-99"
              maskChar={null}
              value={model.phoneNumber.value}
              onChange={(event) => setValue("phoneNumber", event.target.value)}
            >
              {() =>
                <TextField
                placeholder="Phone number"
                fullWidth
                variant="outlined"
                className={classes.input}
                error={model.phoneNumber.isValid ? false : true}
                helperText={model.phoneNumber.isValid ? "" : errors["phoneNumber"]}
              />}
            </InputMask>
            <TextField
              placeholder="Password"
              type={model.password.show ? 'text' : 'password'}
              value={model.password.value}
              onChange={(event) => setValue("password", event.target.value)}
              fullWidth
              variant="outlined"
              autoComplete="off"
              className={classes.input}
              error={model.password.isValid ? false : true}
              helperText={model.password.isValid ? "" : errors["password"]}
              InputProps={{
                endAdornment:(
                  <InputAdornment position="end">
                    <IconButton onClick={handleClickShowPassword} tabIndex="-1">
                      {model.password.show ? <Visibility /> : <VisibilityOff />}
                    </IconButton>
                  </InputAdornment>
                )
              }}
            />
            <TextField
              placeholder="Confirm password"
              type={model.confirmPassword.show ? 'text' : 'password'}
              value={model.confirmPassword.value}
              onChange={(event) => setValue("confirmPassword", event.target.value)}
              fullWidth
              variant="outlined"
              autoComplete="off"
              className={classes.input}
              error={model.confirmPassword.isValid ? false : true}
              helperText={model.confirmPassword.isValid ? "" : errors["confirmPassword"]}
              InputProps={{
                endAdornment:(
                  <InputAdornment position="end">
                    <IconButton onClick={handleClickShowConfirmPassword} tabIndex="-1">
                      {model.confirmPassword.show ? <Visibility /> : <VisibilityOff />}
                    </IconButton>
                  </InputAdornment>
                )
              }}
            />
            <div className={classes.buttons}>
              <Button
                onClick={(event) => submitHandler(event)}
                variant="contained"
                fullWidth
                color="primary"
                size="large"
                className={classes.button}
                disabled={submitting}
              >
                Register
              </Button>
              <Button
                onClick={loginHandle}
                variant="text"
                fullWidth
                color="primary"
                size="large"
                className={classes.button}
              >
                Log In
              </Button>
            </div>
          </form>
        : <div className={classes.confirmPasswordContainer}>
            <div className={classes.infoIcon}>i</div>
            <h4>Confirm your account</h4>
            <p className={classes.infoText}>The message with confirmation link was sent to your email</p>
          </div>
        }
      </div>
    </div>
  )
}

const mapDispatchToProps = dispatch => ({
  dispatch
})

export default connect(null, mapDispatchToProps)(Register);