import React from 'react'

export const About = () => (
    <div className="container">
        <div className="jumbotron">
            <div className="container">
                <h1 className="display-4">React App for tutorial</h1>
                <p className="lead">
                    Version <strong>0.3.2</strong>
                </p>
            </div>
        </div>
    </div>
)