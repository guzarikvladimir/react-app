import React, { useState } from 'react'
import { makeStyles } from '@material-ui/core'
import Button from '@material-ui/core/Button'
import TextField from '@material-ui/core/TextField'
import IconButton from '@material-ui/core/IconButton'
import InputAdornment from "@material-ui/core/InputAdornment"
import { logIn } from '../redux/modules/authorization/authActions.js'
import { useHistory } from 'react-router-dom'
import { connect } from 'react-redux'

import Visibility from '@material-ui/icons/Visibility'
import VisibilityOff from '@material-ui/icons/VisibilityOff'

import styles from '../content/styles/authorization/authorizationStyles.js'

const useStyles = makeStyles(styles)

const errors = { }
const Login = (props) => {
  const [loginModel, setModel] = useState({
    email: { value: "", isValid: true },
    password: { value: "", isValid: true, show: false }
  })
  const [submitting, setSubmitting] = useState(false)

  const classes = useStyles()
  const history = useHistory()
  const { dispatch } = props

  const logInHandler = event => {
    event.preventDefault()
    if (!isValid()) {
      return
    }

    setSubmitting(true)
    dispatch(logIn(loginModel.email.value, loginModel.password.value)).then(() => {
      const redirect = getReturnUrl()
      history.replace(redirect ?? '/')
    }).catch(({response}) => {
      const serverErrors = response.data.errors ?? response.data
      Object.getOwnPropertyNames(serverErrors).forEach(errKey=> {
        const lowerKey = errKey.toLowerCase()
        loginModel[lowerKey].isValid = false
        errors[lowerKey] = serverErrors[errKey]
      })

      setSubmitting(false)
      setModel({ ...loginModel })
    })
  }

  const getReturnUrl = () => {
    const redirect = new URLSearchParams(window.location.search).get('returnUrl')
    if (!redirect || redirect.indexOf('login') > 0) {
        return null
    }

    return redirect
  }

  const isValid = () => {
    let valid = true
    Object.getOwnPropertyNames(loginModel).forEach((key) => {
      if (!loginModel[key].value || loginModel[key].value.trim() === "") {
        loginModel[key].isValid = false
        valid = false
        errors[key] = "Field can not be empty"
      }
    })

    if (valid) {
      return true
    }

    setModel({ ...loginModel })
    return false
  }

  const setValue = (type, value) => {
    loginModel[type].value = value
    loginModel[type].isValid = true
    errors[type] = null

    setModel({ ...loginModel })
  }

  const registerHandle = () => {
    let query = getReturnUrl()
    if (query) {
      history.replace(`/register?returnUrl=${encodeURIComponent(query)}`)
    } else {
      history.replace('/register')
    }
  }

  const handleClickShowPassword = () => {
    loginModel.password.show = !loginModel.password.show

    setModel({ ...loginModel });
  }

  return (
    <div className={classes.container}>
      <div className={classes.headerContainer}>
        <h4>Log in to your account</h4>
        <p className={classes.infoText}>Please, log in to your account</p>
      </div>
      <div className={classes.formContainer}>
        <form onSubmit={logInHandler} className={classes.form}>
          <TextField
            placeholder="Email"
            value={loginModel.email.value}
            onChange={(event) => setValue("email", event.target.value)}
            fullWidth
            variant="outlined"
            className={classes.input}
            error={loginModel.email.isValid ? false : true}
            helperText={loginModel.email.isValid ? "" : errors["email"]}
          />
          <TextField
            placeholder="Password"
            type={loginModel.password.show ? 'text' : 'password'}
            value={loginModel.password.value}
            onChange={(event) => setValue("password", event.target.value)}
            fullWidth
            variant="outlined"
            autoComplete="off"
            className={classes.input}
            error={loginModel.password.isValid ? false : true}
            helperText={loginModel.email.isValid ? "" : errors["password"]}
            InputProps={{
              endAdornment:(
                <InputAdornment position="end">
                  <IconButton onClick={handleClickShowPassword} tabIndex="-1">
                    {loginModel.password.show ? <Visibility /> : <VisibilityOff />}
                  </IconButton>
                </InputAdornment>
              )
            }}
          />
          <div className={classes.buttons}>
            <Button
              onClick={(event) => logInHandler(event)}
              variant="contained"
              fullWidth
              color="primary"
              size="large"
              className={classes.button}
              disabled={submitting}
            >
              Log in
            </Button>
            <Button
              onClick={registerHandle}
              variant="text"
              fullWidth
              color="primary"
              size="large"
              className={classes.button}
            >
              Register
            </Button>
          </div>
        </form>
      </div>
    </div>
  )
}

const mapDispatchToProps = dispatch => ({
  dispatch
})

export default connect(null, mapDispatchToProps)(Login);