import React, { useContext, useEffect, useRef } from 'react'
import { makeStyles } from '@material-ui/core/styles'
import Grid from '@material-ui/core/Grid'
import Container from '@material-ui/core/Container'
import { CurrencyTable } from '../components/currencyExchanger/CurrencyTable'
import { CurrencyContext } from '../context/currency/currencyContext'
import { Loader } from '../components/common/Loader'
import { CurrencyPanel } from '../components/currencyExchanger/CurrencyPanel'
import { useState } from 'react'
import { Error } from '../components/common/Error'
import { ContextMenu } from '../components/common/ContextMenu'
import { AlertContext } from '../context/alert/alertContext'
import { Alert } from '../components/common/Alert'

const useStyles = makeStyles((theme) => ({
    root: {
      flexGrow: 1
    },
    paper: {
      padding: theme.spacing(0),
      textAlign: 'center',
      justifyContent: 'space-between',
      color: theme.palette.text.secondary,
    },
    info: {
        padding: '.5rem'
    }
}));

const tableId = "currencyTable"

export const Exchanger = () => {
    const {currencies, loading, fetchCurrencies} = useContext(CurrencyContext)
    const {show} = useContext(AlertContext)
    const [selected, setSelected] = useState()
    const [error, SetError] = useState()
    const panelRef = useRef()
    const classes = useStyles();
    const alertMessage = "No one row was selected."

    const contextMenuItems = [
        { label: 'Set from', onSelect: onSetFromSelect },
        { label: 'Set to', onSelect: onSetToSelect }
    ]

    function onSetFromSelect() {
        if (!selected) {
            show(alertMessage)
        } else {
            panelRef.current.changeType(selected, 0)
            setSelected("")
        }
    }

    function onSetToSelect() {
        if (!selected) {
            show(alertMessage)
        } else {
            panelRef.current.changeType(selected, 1)
            setSelected("")
        }
    }

    function onMenuClose() {
        setSelected("")
    }

    const rowSelected = name => {
        if (name === selected) {
            name = ""
        }
        
        setSelected(name)
    }

    useEffect(() => {
        async function fetchData() {
            try {
                await fetchCurrencies()
            } catch (error) {
                SetError(error)
            }
        }

        fetchData()
        // eslint-disable-next-line
    }, [])

    return (
        <Container maxWidth="lg" className={classes.root}>
            <Grid container spacing={3}>
                {error
                    ? <Error error={error}/>
                    : loading
                        ? <Loader />
                        : <>
                            <Grid item md={4} xs={12}>
                                <CurrencyPanel ref={panelRef} />
                            </Grid>
                            <Grid item md={8} xs={12}>
                                <ContextMenu menuItems={contextMenuItems} onClose={onMenuClose} targetId={tableId}>
                                    <CurrencyTable 
                                        currencies={currencies} 
                                        selected={selected} 
                                        selectRow={rowSelected}
                                        tableId={tableId}
                                    />
                                </ContextMenu>
                                <small>Right click on the table to call context menu</small>
                            </Grid>
                            <Alert />
                        </>
                }
            </Grid>
        </Container>
    )
}