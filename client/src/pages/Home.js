import React, { useContext, useEffect } from 'react'
import { Form } from '../components/notes/Form'
import { Notes } from '../components/notes/Notes'
import { FirebaseContext } from '../context/firebase/firebaseContext'
import { Loader } from '../components/common/Loader'
import { Alert } from '../components/common/Alert'
import { Modal } from '../components/common/Modal'

export const Home = () => {
    const {loading, notes, fetchNotes, removeNote, updateNote} = useContext(FirebaseContext)

    useEffect(() =>{
        fetchNotes()
        // eslint-disable-next-line
    }, [])

    return (
        <div className="container-sm">
            <Modal 
                title={"Are you sure you want to delete this note?"} 
                confirm={removeNote}
            />
            <Alert/>
            <Form/>

            <hr/>

            {loading 
                ? <Loader/>
                : <Notes notes={notes} updateNote={updateNote}/>}
        </div>
    )
}