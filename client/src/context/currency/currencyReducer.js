import { SHOW_LOADER } from '../types'
import { FETCH_RATES } from './currencyTypes'

const handlers = {
    [SHOW_LOADER]: state => ({...state, loading: true}),
    [FETCH_RATES]: (state, payload) => ({...state, currencies: payload.currencies, rates: payload.rates, loading: false}),
    DEFAULT: state => state
}

export const currencyReducer = (state, action) => {
    const handler = handlers[action.type] || handlers.DEFAULT
    
    return handler(state, action)
}