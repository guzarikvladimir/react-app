import React, { useReducer } from 'react'
import { CurrencyContext } from './currencyContext'
import axios from 'axios'
import { FETCH_RATES } from './currencyTypes'
import { SHOW_LOADER } from '../types'
import { currencyReducer } from './currencyReducer'

//const URL = 'http://data.fixer.io/api/latest?access_key=a80db07a7eb7447aaa8b0dd6757b9e01&format=1'
const URL = 'https://api.exchangeratesapi.io/latest'

const initialState = {
    currencies: [],
    rates: [],
    loading: false
}

export const CurrencyState = ({children}) => {
    const [state, dispatch] = useReducer(currencyReducer, initialState)
    
    const fetchCurrencies = async () => {
        dispatch({type: SHOW_LOADER})
        let response = await axios.get(URL)

        let currencies
        try {
            let currencyKeys = Object.getOwnPropertyNames(response.data.rates || [])
            currencies = currencyKeys.map(key => {
                return {
                    name: key,
                    rate: response.data.rates[key]
                }
            })
        } catch (error) {
            throw new Error("Couldn't load data")
        }

        dispatch({type: FETCH_RATES, currencies, rates: response.data.rates})
    }

    // use additional dictionaries for converting
    const convert = (amount, from, to) => {
        if (state.rates.length === 0) {
            return 1
        }

        return amount * state.rates[to] / state.rates[from]
    }

    return (
        <CurrencyContext.Provider value={{
            fetchCurrencies, convert,
            currencies: state.currencies,
            rates: state.rates,
            loading: state.loading
        }}>
            {children}
        </CurrencyContext.Provider>
    )
}