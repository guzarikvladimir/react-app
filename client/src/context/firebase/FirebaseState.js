import React, { useReducer } from 'react'
import axios from 'axios'
import { FirebaseContext } from './firebaseContext'
import { firebaseReducer } from './firebaseReducer'
import { REMOVE_NOTE, ADD_NOTE, FETCH_NOTES, UPDATE_NOTE } from './firebaseTypes'
import { SHOW_LOADER } from '../types'

//const url = process.env.REACT-APP_DB_URL
const url = 'https://react-app-aa059.firebaseio.com'

// Idea: to wrap app with the state, 
// to give access to child elements to the context's state
export const FirebaseState = ({children}) => {
    var greatestOrder = -1
    const initialState = {
        notes: [],
        loading: false
    }
    
    const [state, dispatch] = useReducer(firebaseReducer, initialState)
    
    const showLoader = () => dispatch({type: SHOW_LOADER})

    const fetchNotes = async () => {
        showLoader()
        const res = await axios.get(`${url}/notes.json`)
        
        const payload = Object.keys(res.data || []).map(key => {
            return {
                ...res.data[key],
                id: key,
                date: new Date(res.data[key].date)
            }
        }).sort((a, b) => {
            if (a.order > b.order) {
                return 1
            }
            if (a.order < b.order) {
                return -1;
            }

            return 0;
        })

        dispatch({type: FETCH_NOTES, payload})
    }

    const updateGreatesOrder = () => {
        greatestOrder++
        let last = 0
        if (state.notes.length > 0) {
            last = Math.max.apply(null, state.notes.map(note => note.order)) + 1
        }
        // additional check 
        greatestOrder = Math.max(last, greatestOrder)

        return greatestOrder;
    }

    const addNote = async title => {
        const note = {
            title,
            date: new Date(),
            checked: false,
            order: updateGreatesOrder()
        }
        
        try {
            const res = await axios.post(`${url}/notes.json`, note)
            const payload = {
                ...note,
                id: res.data.name
            }
            dispatch({type: ADD_NOTE, payload})
        } catch (e) {
            throw new Error(e.message)
        }
    }

    const removeNote = async id => {
        await axios.delete(`${url}/notes/${id}.json`)
        
        let removeNote = state.notes.find(note => note.id === id);
        let nextNotes = state.notes.splice(removeNote.order + 1)
        nextNotes = nextNotes.map((note, index) => {
            note.order = index + removeNote.order
            return note
        })
        nextNotes.forEach(note => {
            updateNote(note, false)
        })

        dispatch({
            type: REMOVE_NOTE, 
            payload: [...state.notes.slice(0, removeNote.order), ...nextNotes]
        })
    }

    const updateNote = async (note, updateState = true) => {
        if (updateState) {
            dispatch({type: UPDATE_NOTE, payload: note})
        }
        
        try {
            await axios.put(`${url}/notes/${note.id}.json`, note)
        }
        catch {
            dispatch({type: UPDATE_NOTE, payload: note})
        }
    }

    return (
        <FirebaseContext.Provider value={{
            showLoader, addNote, removeNote, fetchNotes, updateNote,
            loading: state.loading,
            notes: state.notes
        }}>
            {children}
        </FirebaseContext.Provider>
    )
}