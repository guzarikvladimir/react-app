import { FETCH_NOTES, REMOVE_NOTE, ADD_NOTE, UPDATE_NOTE } from "./firebaseTypes"
import { SHOW_LOADER } from "../types"

const handlers = {
    [SHOW_LOADER]: state => ({...state, loading: true}),
    [ADD_NOTE]: (state, {payload}) => ({
        ...state, 
        notes: [...state.notes, payload]}),
    [FETCH_NOTES]: (state, {payload}) => ({...state, notes: payload, loading: false}),
    [REMOVE_NOTE]: (state, {payload}) => ({
        ...state,
        notes: payload}),
    [UPDATE_NOTE]: (state, {payload}) => ({
        ...state, 
        notes: state.notes.map(note => note.id !== payload.id ? note : payload)}),
    DEFAULT: state => state
}

export const firebaseReducer = (state, action) => {
    const handle = handlers[action.type] || handlers.DEFAULT

    return handle(state, action)
}