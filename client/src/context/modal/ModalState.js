import React, { useReducer } from 'react'
import { ModalContext } from './modalContext'
import { modalReducer } from './modalReducer'
import { SET_TARGET, CLEAR_TARGET } from './modalTypes'

export const ModalState = ({children}) => {
    const [state, dispatch] = useReducer(modalReducer)

    const setTarget = (target) => {
        dispatch({type: SET_TARGET, payload: target})
    }

    const clearTarget = () => {
        dispatch({type: CLEAR_TARGET})
    }
    
    return (
        <ModalContext.Provider value={{
            setTarget, clearTarget,
            modal: state
        }}>
            {children}
        </ModalContext.Provider>
    )
}