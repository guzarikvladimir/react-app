import { SET_TARGET, CLEAR_TARGET } from "./modalTypes"

const handlers = {
    [SET_TARGET]: (state, {payload}) => ({...state, target: payload}),
    [CLEAR_TARGET]: state => ({...state, target: null}),
    DEFAULT: state => state
}

export const modalReducer = (state, action) => {
    const handle = handlers[action.type] || handlers.DEFAULT
    
    return handle(state, action)
}