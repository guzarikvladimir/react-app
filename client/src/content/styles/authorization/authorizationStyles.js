const styles = {
  container: {
    position: 'absolute',
    width: '100%',
    height: 'calc(100% - 90px)',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'column'
  },
  headerContainer: {
    width: '400px',
    padding: '2.5rem',
    textAlign: 'center',
  },
  infoText: {
    color: 'RGB(144, 144, 129, 1)',
    margin: '0'
  },
  formContainer: {
    width: '400px',
    padding: '2.5rem',
    boxShadow: '0px 0px 10px 1px rgba(222,222,222,.5)',
    alignItems: 'center',
    display: 'flex'
  },
  registeredFormContainer: {
    minHeight: '556px'
  },
  form: {
    width: '100%'
  },
  input: {
    marginBottom: '1rem'
  },
  button: {
    marginTop: '1rem'
  },
  confirmPasswordContainer: {
    textAlign: 'center'
  },
  infoIcon: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    width: '50px',
    height: '50px',
    margin: 'auto',
    marginBottom: '15px',
    fontSize: '26pt',
    fontWeight: '600',
    color: 'RGB(56, 144, 247)',
    borderRadius: '50%',
    border: '4px solid RGB(56, 144, 247)',
    backgroundColor: 'RGB(232, 247, 254)',
    cursor: 'default',
    userSelect: 'none'
  }
}

export default styles