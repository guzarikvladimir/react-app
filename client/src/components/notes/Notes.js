import React, { useContext } from 'react'
import { ModalContext } from '../../context/modal/modalContext'
import { DragDropContext, Droppable, Draggable } from 'react-beautiful-dnd'

export const Notes = ({notes, updateNote}) => {
    const {setTarget} = useContext(ModalContext)
    
    const onDragEnd = result => {
        const {destination, source} = result
        
        if (!destination) {
            return;
        }

        if (destination.index === source.index) {
            return;
        }

        let dragNote = notes[source.index]
        dragNote.order = destination.index;
        notes.splice(source.index, 1)
        notes.splice(destination.index, 0, dragNote)

        let start = destination.index > source.index 
            ? source.index 
            : destination.index
        let nextNotes = notes.slice(start)
        nextNotes.forEach((note, index) => {
            note.order = index + start
            updateNote(note)
        })
    }

    return (
        <DragDropContext onDragEnd={onDragEnd}>
            <Droppable droppableId={"1"}>
                {provided => (
                    <ul
                        className="list-group" 
                        ref={provided.innerRef}
                        {...provided.droppableProps}
                    >
                        {notes.map((note, index) => (
                            <Draggable 
                                draggableId={note.id} 
                                index={index} 
                                key={note.id}
                            >
                                {provided => (
                                    <li 
                                        {...provided.draggableProps}
                                        ref={provided.innerRef}
                                        className="list-group-item note"
                                    >
                                        <div className="custom-control custom-checkbox">
                                            <input 
                                                type="checkbox" 
                                                onChange={() => {note.checked=!note.checked; updateNote(note)}} 
                                                checked={`${note.checked ? 'checked' : ''}`} 
                                                className="custom-control-input" 
                                                id={note.id}
                                            />
                                            <label className={`custom-control-label ${note.checked ? 'note-done' : ''}`} htmlFor={note.id}>{note.title}</label>
                                            <small>created: {note.date.toLocaleDateString()}</small>
                                        </div>
                                            
                                        <div style={{display:"flex"}}>
                                            <div {...provided.dragHandleProps} className="drag-icon">
                                                <svg className="bi bi-arrow-up-down" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                    <path fillRule="evenodd" d="M11 3.5a.5.5 0 01.5.5v9a.5.5 0 01-1 0V4a.5.5 0 01.5-.5z" clipRule="evenodd"/>
                                                    <path fillRule="evenodd" d="M10.646 2.646a.5.5 0 01.708 0l3 3a.5.5 0 01-.708.708L11 3.707 8.354 6.354a.5.5 0 11-.708-.708l3-3zm-9 7a.5.5 0 01.708 0L5 12.293l2.646-2.647a.5.5 0 11.708.708l-3 3a.5.5 0 01-.708 0l-3-3a.5.5 0 010-.708z" clipRule="evenodd"/>
                                                    <path fillRule="evenodd" d="M5 2.5a.5.5 0 01.5.5v9a.5.5 0 01-1 0V3a.5.5 0 01.5-.5z" clipRule="evenodd"/>
                                                </svg>
                                            </div>

                                            <button
                                                type="button"
                                                className="btn btn-outline-danger btn-sm"
                                                data-target="#confirmModal" data-toggle="modal"
                                                onClick={() => setTarget(note.id)}
                                            >
                                                &times;
                                            </button>
                                        </div>
                                    </li>
                                )}
                            </Draggable>
                        ))}
                        {provided.placeholder}
                    </ul>
                )}
            </Droppable>
        </DragDropContext>
    )
}