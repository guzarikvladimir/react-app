import React from 'react'
import { makeStyles } from '@material-ui/core'

import emptyProfile from '../../content/assets/empty_profile.png'

const useStyles = makeStyles({
  avatarImg: {
    width: '50px',
    height: '50px',
    borderRadius: '50%',
    marginRight: '10px'
  },
  avatarImgSmall: {
    width: '40px',
    height: '40px',
    borderRadius: '50%',
    marginBottom: '25px',
    marginRight: '10px'
  }
})

export const Avatar = (props) => {
  const classes = useStyles()
  const { contact, size } = props

  return (
    <>
      <img
        src={contact.profileImg ?? emptyProfile}
        className={size === "small" ? classes.avatarImgSmall : classes.avatarImg}
        alt={"profile"}
      />
      {contact.status
        ? <svg width="12" height="12" className={classes.status}>
            <circle r="6" cx="6" cy="6" fill="yellowgreen" />
          </svg>
        : ""}
    </>
  )
}