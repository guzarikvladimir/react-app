import React, { useState } from 'react'
import { InputBase, Badge, MenuItem, ClickAwayListener, Paper, MenuList } from '@material-ui/core'
import { makeStyles } from '@material-ui/core'
import { CardActionArea } from '@material-ui/core'
import { IconButton } from '@material-ui/core'
import { Divider } from '@material-ui/core'
import { connect } from 'react-redux'
import { dateToShortString } from '../../functions/formatting'
import { useHistory } from 'react-router-dom'
import { searchContact, createDialog } from '../../redux/modules/chat/chatActions'

import PeopleIcon from '@material-ui/icons/People'
import CreateIcon from '@material-ui/icons/Create'
import SearchIcon from '@material-ui/icons/Search'

import emptyProfile from '../../content/assets/empty_profile.png'

const useStyles = makeStyles({
  header: {
    margin: '0 1rem',
    height: '60px',
    justifyContent: 'space-between',
    display: 'flex',
    alignItems: 'center'
  },
  editIcon: {
    width: '20px',
    height: '20px'
  },
  search: {
    margin: '1rem',
    padding: '.2rem',
    alignItems: 'center',
    backgroundColor: 'RGB(247, 248, 249)',
    borderRadius: '2px'
  },
  searchInput: {
    padding: 0
  },
  searchIcon : {
    margin: 'auto .3rem',
    color: 'RGB(207, 208, 209)'
  },
  contacts: {
    margin: 'auto 1rem'
  },
  row: {
    display: 'flex',
    margin: '1rem 0',
    width: '100%',
    padding: '2px',
    maxHeight: '80px'
  },
  avatarImg: {
    width: '50px',
    height: '50px',
    borderRadius: '50%',
    marginRight: '10px'
  },
  avatar: {
    position: 'relative'
  },
  infoBox: {
    width: 'calc(100% - 60px)'
  },
  contactTitle: {
    display: 'flex',
    justifyContent: 'space-between'
  },
  time: {
    color: 'RGB(166, 166, 166)',
    margin: '0'
  },
  lastMessage: {
    overflow: 'hidden',
    whiteSpace: 'nowrap',
    textOverflow: 'ellipsis',
    color: 'RGB(166, 166, 166)',
    marginRight: '25px',
  },
  status: {
    position: 'absolute',
    bottom: '0',
    left: '0',
    marginLeft: '35px',
    border: '2px solid white',
    borderRadius: '50%'
  },
  unreadMessages: {
    position: 'absolute',
    bottom: '15px',
    right: '15px'
  },
  dropdown: {
    position: 'absolute',
    zIndex: '1',
    minWidth: '220px',
    maxHeight: '300px',
    overflow: 'auto'
  },
  menuList: {
    padding: '0'
  },
  dropdownItem: {
    padding: '5px',
    display: 'flex',
    alignItems: 'center'
  },
  dropdownItemAvatar: {
    width: '40px',
    height: '40px',
    borderRadius: '50%',
    marginRight: '5px'
  },
  dropdownItemText: {
    display: 'flex',
    flexDirection: 'column',
  }
})

/*
status:
0: offline,
1: online
*/

const Contacts = (props) => {
  const classes = useStyles()
  const [contacts, setContacts] = useState([])
  const [target, setTarget] = useState()
  const { dialogs, dispatch } = props
  const history = useHistory()

  function selectDialog(dialogId) {
    history.push(`/chat/${dialogId}`)
  }

  function searchContactHandler(event) {
    setTarget(event.currentTarget)
    searchContact(event.target.value).then(({data}) => {
      setContacts(data)
    })
  }

  function createDialogHandler(user) {
    setContacts([])
    target.value = ''
    
    const dialog = dialogs.find(d => d.user.id === user.id)
    if (dialog) {
      selectDialog(dialog.id)
    } else {
      let newDialogId = dispatch(createDialog(user))
      history.push(`/chat/${newDialogId}`)
    }
  }

  function handleCloseAway() {
    setContacts([])
  }

  return (
    <>
      <div className={classes.header}>
        <div>
          <PeopleIcon /> Contacts
        </div>
        <div>
          <IconButton size="small">
            <CreateIcon />
          </IconButton>
        </div>
      </div>
      <Divider />
      <div className={classes.contactsPaper}>
        <div className={classes.search}>
          <InputBase
            fullWidth
            className={classes.searchInput}
            variant="filled"
            placeholder="Search for a contact"
            onChange={event => searchContactHandler(event)}
            startAdornment={
              <SearchIcon className={classes.searchIcon} />
            }
          />
          {contacts && contacts.length> 0
          ? <Paper className={classes.dropdown}>
              <ClickAwayListener onClickAway={handleCloseAway}>
                <MenuList role="menu" className={classes.menuList}>
                  {contacts.map((item, index) => {
                    return (
                        <MenuItem
                          key={index}
                          onClick={() => createDialogHandler(item)}
                          className={classes.dropdownItem}
                        >
                          <img
                            src={item.profileImg ?? emptyProfile}
                            className={classes.dropdownItemAvatar}
                            alt="profile"
                          />
                          <div className={classes.dropdownItemText}>
                            <strong>{item.name}</strong>
                            <small>{item.email}</small>
                          </div>
                        </MenuItem>
                    )
                  })}
                </MenuList>
              </ClickAwayListener>
            </Paper>
          : ""}
        </div>
        <div className={classes.contacts}>
          {dialogs
            ? dialogs.map((dialog, index) => {
              return (
                <CardActionArea
                  className={classes.row} 
                  key={index}
                  centerRipple
                  onClick={() => selectDialog(dialog.id)}
                >
                   <div className={classes.avatar}>
                    <img
                      src={dialog.user.profileImg ?? emptyProfile}
                      className={classes.avatarImg}
                      alt="profile"
                    />
                    {dialog.user.status?.status
                      ? <svg width="12" height="12" className={classes.status}>
                          <circle r="6" cx="6" cy="6" fill="yellowgreen" />
                        </svg>
                      : ""}
                  </div>
                  <div className={classes.infoBox}>
                    <div className={classes.contactTitle}>
                      <strong>{dialog.user.name}</strong>
                        {
                          dialog.lastMessage
                            ? <div className={classes.time}>{dateToShortString(dialog.lastMessage.createdDate)}</div>
                            : ""}
                    </div>
                    {dialog.lastMessage
                      ? <div className={classes.lastMessage}>
                          {dialog.lastMessage.text}
                          {dialog.unreadMessagesCount
                          ? <Badge 
                              color="secondary" 
                              badgeContent={dialog.unreadMessagesCount}
                              className={classes.unreadMessages}
                            ></Badge>
                          : ""
                          }
                        </div>
                      : ""
                    }
                  </div>
                </CardActionArea>
              )
            })
            : <div>No contacts.</div>
          }
        </div>
      </div>
    </>
  )
}

const mapStateToProps = state => ({
  dialogs: state.chat.dialogs
})

const mapDispatchToProps = dispatch => ({
  dispatch
})

export default connect(mapStateToProps, mapDispatchToProps)(Contacts)