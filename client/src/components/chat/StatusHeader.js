import React from 'react'
import { IconButton, makeStyles, Hidden } from '@material-ui/core'
import MenuOutlinedIcon from '@material-ui/icons/MenuOutlined'
import MoreHorizIcon from '@material-ui/icons/MoreHoriz'
import { connect } from 'react-redux'
import { dateToStatus } from '../../functions/formatting'

const useStyles = makeStyles({
  header: {
    height: '60px',
    justifyContent: 'center',
    display: 'flex',
    alignItems: 'center',
    textAlign: 'center',
    flexDirection: 'row'
  },
  headerBox: {
    alignItems: 'center'
  },
  hidden: {
    marginLeft: '3rem',
    marginRight: 'auto',
    visibility: 'hidden'
  },
  statusText: {
    color: 'RGB(193, 193, 193)'
  },
  statusCircle: {
    margin: 'auto 8px auto -8px'
  },
  more: {
    color: 'RGB(177, 177, 177)',
    marginLeft: 'auto'
  },
  contactsToggler : {
    position: 'absolute',
    left: '1rem'
  }
})

const StatusHeader = (props) => {
  const classes = useStyles()
  const { selectedDialogUser, handleDrawerToggle } = props

  return (
    <div className={classes.header}>
      <Hidden smUp>
        <IconButton
          color="inherit"
          aria-label="open drawer"
          onClick={handleDrawerToggle}
          className={classes.contactsToggler}
        >
          <MenuOutlinedIcon />
        </IconButton>
      </Hidden>
      <div className={classes.hidden}></div>
      {selectedDialogUser
        ? <div className={classes.headerBox}>
            <strong>{selectedDialogUser.name}</strong>
            <div>
              {selectedDialogUser.status
              ? selectedDialogUser.status.status
                  ? <>
                      <svg width="8" height="8" className={classes.statusCircle}>
                        <circle r="4" cx="4" cy="4" fill="yellowgreen" />
                      </svg>
                      <span className={classes.statusText}>online</span>
                    </>
                  : <span className={classes.statusText}>
                      {dateToStatus(selectedDialogUser.status.lastSeen)}
                    </span>
              : ""}
            </div>
          </div>
        : ""
      }
      <div className={classes.more}>
        <IconButton>
          <MoreHorizIcon />
        </IconButton>
      </div>
    </div>
  )
}

const mapStateToProps = state => ({
  selectedDialogUser: state.chat.selectedDialogUser
})

export default connect(mapStateToProps)(StatusHeader)