import React from 'react'
import { makeStyles } from '@material-ui/core'
import { IconButton } from '@material-ui/core'
import { TextField } from '@material-ui/core'
import { dateToString } from '../../functions/formatting'
import { connect } from 'react-redux'
import { useState } from 'react'
import { sendMessage, getDialog } from '../../redux/modules/chat/chatActions'
import SendIcon from '@material-ui/icons/Send'

import emptyProfile from '../../content/assets/empty_profile.png'

import '../../content/styles/chat/scrollBar.scss'

const useStyles = makeStyles({
  dialog: {
    display: 'flex',
    height: 'calc(100% - 61px)',
    padding: '0 1rem 1rem 1rem',
    flexDirection: 'column'
  },
  messages: {
    padding: '0 .5rem .5rem .5rem',
    display: 'flex',
    height: '100%',
    overflow: 'auto',
    flexDirection: 'column-reverse'
  },
  messageBox: {
    display: 'flex',
    margin: '.5rem 0',
    maxWidth: '700px'
  },
  avatarImg: {
    width: '40px',
    height: '40px',
    borderRadius: '50%',
    marginBottom: '25px',
    marginRight: '10px'
  },
  avatar: {
    position: 'relative',
    alignSelf: 'flex-end',
  },
  status: {
    position: 'absolute',
    bottom: '0',
    left: '0',
    marginLeft: '30px',
    marginBottom: '25px',
    border: '2px solid white',
    borderRadius: '50%'
  },
  messageText: {
    border: '1px solid RGB(245, 245, 245)',
    borderRadius: '20px 20px 20px 0px',
    padding: '1rem'
  },
  dateText: {
    color: 'RGB(194, 194, 194)'
  },
  messageBar: {
    display: 'block',
    width: '100%'
  },
  outgoing: {
    backgroundColor: 'RGB(54, 116, 255)',
    color: 'white'
  },
  dialogNotSelected: {
    display: 'flex',
    height: '100%',
    color: 'RGB(166, 166, 166)',
    justifyContent: 'center',
    alignItems: 'center'
  }
})

const Dialog = (props) => {
  const classes = useStyles()
  const [message, setMessage] = useState("")
  const { currentUser, selectedDialog, selectedDialogUser, dispatch } = props

  function messageInput(event) {
    setMessage(event.target.value)
  }

  function handleKeyDown(event) {
    if (event.keyCode === 13) {
      sendMessageHandler()
      event.preventDefault()
    }
  }

  function sendMessageHandler() {
    if (!message) {
      return
    }

    dispatch(sendMessage(message, selectedDialog.id, selectedDialogUser.id)).then(() => {
      return dispatch(getDialog(selectedDialog.id))
    }).then(() => {
      setMessage("")
    })
  }

  return (
    <>
      <div className={classes.dialog}>
        {selectedDialog
          ? <>
              <div className={classes.messages}>
                {selectedDialog.messages.map((msg, index) => {
                  return (
                    <div key={index} className={classes.messageBox}>
                      <div className={classes.avatar}>
                        <img 
                          className={classes.avatarImg}
                          src={(msg.userId === currentUser.id ? currentUser.profileImg : selectedDialogUser.profileImg) ?? emptyProfile}
                          alt="profile"
                        />
                        {msg.userId === currentUser.id || selectedDialogUser.status.status
                        ? <svg width="12" height="12" className={classes.status}>
                            <circle r="6" cx="6" cy="6" fill="yellowgreen" />
                          </svg>
                        : ""}
                      </div>
                      <div>
                        <div className={`${classes.messageText} ${msg.userId === currentUser.id ? classes.outgoing : ""}`}>
                          {msg.text}
                        </div>
                        <small className={classes.dateText}>{dateToString(msg.createdDate)}</small>
                      </div>
                    </div>
                  )
                })}
              </div>
              <div className={classes.messageBar}>
                <form onSubmit={() => sendMessageHandler()} id="text">
                  <TextField
                      form="text"
                      fullWidth
                      size="small"
                      variant="outlined"
                      multiline
                      value={message}
                      placeholder="Input message text ..."
                      onChange={(event) => messageInput(event)}
                      onKeyDown={(event) => handleKeyDown(event)}
                      InputProps={{
                        endAdornment: 
                          <IconButton 
                            onClick={() => sendMessageHandler()}
                            size="small"
                          >
                            <SendIcon />
                          </IconButton>
                      }}
                    />
                </form>
              </div>
            </>
          : <div className={classes.dialogNotSelected}>
              No dialog selected
            </div>
        }
      </div>
    </>
  )
}

const mapStateToProps = (state) => ({
  currentUser: state.auth.currentUser,
  selectedDialog: state.chat.selectedDialog,
  selectedDialogUser: state.chat.selectedDialogUser
})

const mapDispatchToProps = (dispatch) => ({
  dispatch
})

export default connect(mapStateToProps, mapDispatchToProps)(Dialog)