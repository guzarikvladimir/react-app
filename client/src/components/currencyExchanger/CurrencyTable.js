import React, { useState } from 'react'
import Table from '@material-ui/core/Table'
import TableBody from '@material-ui/core/TableBody'
import TableCell from '@material-ui/core/TableCell'
import TableContainer from '@material-ui/core/TableContainer'
import TableHead from '@material-ui/core/TableHead'
import TableRow from '@material-ui/core/TableRow'
import Paper from '@material-ui/core/Paper'
import { TableSortLabel } from '@material-ui/core'
import { normalizeValue } from '../../functions/formatting'
import { stableSort, getComparator } from '../../functions/collections'

const styles = {
    table: {
        maxHeight: 'calc(100vh - 130px)'
    },
    visuallyHidden: {
        border: 0,
        clip: 'rect(0 0 0 0)',
        height: 1,
        margin: -1,
        overflow: 'hidden',
        padding: 0,
        position: 'absolute',
        top: 20,
        width: 1,
      }
}

const headCells = [
    { id: 'name', numeric: false, label: 'Name' },
    { id: 'rate', numeric: true, label: 'Rate' }
]

export const CurrencyTable = (props) => {
    const [orderBy, setOrderBy] = useState('name')
    const [order, SetOrder] = useState('asc')
    
    const handleRequestSort = (headCellId) => {
        const isAsc = orderBy === headCellId && order === 'asc'
        SetOrder(isAsc ? 'desc' : 'asc')
        setOrderBy(headCellId)
        props.selectRow("")
    }

    return (
        <TableContainer component={Paper} style={styles.table}>
            <Table stickyHeader size="small">
                <TableHead>
                    <TableRow>
                        {headCells.map(headCell => (
                            <TableCell 
                                key={headCell.id}
                                align={headCell.numeric ? 'right' : 'left'}
                                sortDirection={orderBy === headCell.id ? order : false}
                            >
                                <TableSortLabel 
                                    active={orderBy === headCell.id}
                                    direction={orderBy === headCell.id ? order : 'asc'}
                                    onClick={() => handleRequestSort(headCell.id)}
                                >
                                    {headCell.label}
                                    {orderBy === headCell.id 
                                        ? (<span style={styles.visuallyHidden}>
                                            {order === 'desc' ? 'sorted descending' : 'sorted ascending'}
                                        </span>)
                                        : null
                                    }
                                </TableSortLabel>
                            </TableCell>
                        ))}
                    </TableRow>
                </TableHead>
                
                <TableBody id={props.tableId}>
                    {stableSort(props.currencies, getComparator(order, orderBy))
                        .map(row => (
                            <TableRow onClick={() => props.selectRow(row.name)} selected={props.selected === row.name} hover key={row.name}>
                                <TableCell component="th" scope="row">{row.name}</TableCell>
                                <TableCell align="right">{normalizeValue(row.rate)}</TableCell>
                            </TableRow>
                        ))}
                </TableBody>
            </Table>
        </TableContainer>
    )
}