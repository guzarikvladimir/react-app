import React, { useContext, useState, useEffect, forwardRef, useImperativeHandle } from 'react'
import { Paper, makeStyles } from '@material-ui/core'
import { CurrencyContext } from '../../context/currency/currencyContext'
import { normalizeValue } from '../../functions/formatting';
import TextField from '@material-ui/core/TextField'
import Select from '@material-ui/core/Select'
import FormControl from '@material-ui/core/FormControl'

const useStyles = makeStyles((theme) => ({
    paper: {
      padding: theme.spacing(0),
      textAlign: 'center',
      justifyContent: 'space-between',
      color: theme.palette.text.secondary,
    },
    info: {
        padding: '.5rem'
    },
    form: {
        display: 'flex'
    },
    formControl: {
        margin: '.5rem'
    },
    select: {
        width: '80px',
        fontSize: 14
    }
  }));

export const CurrencyPanel = forwardRef((props, ref) => {
    const classes = useStyles()
    const {convert, rates} = useContext(CurrencyContext)
    
    const [first, setFirst] = useState({amount: 1, name: "USD"})
    const [second, setSecond] = useState({amount: 1, name: "RUB"})
    const [rate, setRate] = useState(-1)

    const rows = [
        { item: first },
        { item: second }
    ]

    useImperativeHandle(ref, () => ({
        changeAmount(value, index) {
            if (index === 0) {
                let newSecond = value >= 0 ? convert(value, first.name, second.name) : 0
                setFirst({...first, amount: value})
                setSecond({...second, amount: normalizeValue(newSecond)})
            } else if (index === 1) {
                let newFirst = value >= 0 ? convert(value, second.name, first.name) : 0
                setSecond({...second, amount: value})
                setFirst({...first, amount: normalizeValue(newFirst)})
            }
        },
        changeType(value, index) {
            if (index === 0) {
                let newSecond = convert(first.amount, value, second.name)
                setFirst({...first, name: value})
                setSecond({...second, amount: normalizeValue(newSecond)})
                setRate(normalizeValue(convert(1, value, second.name)))
            } else if (index === 1) {
                let newSecond = normalizeValue(convert(first.amount, first.name, value))
                setSecond({...second, name: value, amount: newSecond})
                setRate(normalizeValue(convert(1, first.name, value)))
            }
        }
    }))

    useEffect(() => {
        let initialRateValue = normalizeValue(convert(1, first.name, second.name))
        setRate(initialRateValue)
        setSecond({...second, amount: initialRateValue})
        // eslint-disable-next-line
    }, [])

    return (
        <Paper className={classes.paper}>
            {rows.map((row, index) => (
                <div key={index} className={classes.form}>
                    <TextField className={classes.formControl} 
                        label="Amount" 
                        type="number"
                        fullWidth 
                        variant="outlined" 
                        InputLabelProps={{shrink: true,}}
                        value={row.item.amount}
                        onChange={event => ref.current.changeAmount(event.target.value, index)} />
                    <div className={classes.formControl}>
                        <FormControl variant="outlined">
                            <Select
                                native
                                value={row.item.name}
                                onChange={event => ref.current.changeType(event.target.value, index)}
                                className={classes.select}
                            >
                                {Object.getOwnPropertyNames(rates)
                                    .map((key, index) => {
                                        return <option key={index} value={key}>{key}</option>
                                    })}
                            </Select>
                        </FormControl>
                    </div>
                </div>
            ))}
            <div className={classes.info}>
                <small>1 {first.name} = </small>
                <h4>{rate} {second.name}</h4>
            </div>
        </Paper>
    )
})