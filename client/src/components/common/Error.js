import React from 'react'

const styles = {
    root : {
        width: '100%',
        textAlign: 'center',
        paddingTop: '2rem'
    }
}

export const Error = ({error}) => {
    return (
        <div style={styles.root}>
            <h6>Error: {error.message}</h6>
        </div>
    )
}