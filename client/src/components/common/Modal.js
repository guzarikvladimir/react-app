import React, { useContext } from 'react'
import { ModalContext } from '../../context/modal/modalContext'

export const Modal = ({title, confirm}) => {    
    const {modal, clearTarget} = useContext(ModalContext)
    const confirmRef = React.useRef()

    const handleConfirm = () => {
        confirmRef.current.disabled = "disabled"
        confirm(modal.target)
            .then(() => {
                clearTarget()
                confirmRef.current.disabled = ""
            })
    }

    return (
        <div className="modal" id="confirmModal" tabIndex="-1" aria-hidden="true">
            <div className="modal-dialog">
                <div className="modal-content">
                    <div className="modal-header">
                        <h5 className="modal-title">{title}</h5>
                        <button type="button" className="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div className="modal-footer">
                        <button type="button" className="btn btn-secondary" data-dismiss="modal">No</button>
                        <button onClick={() => handleConfirm()} ref={confirmRef} type="button" className="btn btn-primary" data-dismiss="modal">Yes</button>
                    </div>
                </div>
            </div>
        </div>
    )
}