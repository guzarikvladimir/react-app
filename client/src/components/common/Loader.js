import React from 'react'

export const Loader = () => (
    <div style={{width: '100%', textAlign: 'center'}}>
        <div className="text-center">
            <div className="spinner-border">
                <span className="sr-only">Loading...</span>
            </div>
        </div>
    </div>
)