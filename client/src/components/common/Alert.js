import React, { useContext } from 'react'
import { AlertContext } from '../../context/alert/alertContext'
import { Snackbar } from '@material-ui/core'
import MuiAlert from '@material-ui/lab/Alert'

export const Alert = () => {
    const {alert, hide} = useContext(AlertContext)
    
    const handleToastClose = (event, reason) => {
        if (reason === 'clickaway') {
          return;
        }
    
        hide();
    };

    return (
        <Snackbar 
            open={alert.visible} 
            autoHideDuration={2000} 
            onClose={handleToastClose}
        >
            <MuiAlert 
                elevation={6}
                variant="filled"
                onClose={handleToastClose} 
                severity={alert.type}
            >
                {alert.text}
            </MuiAlert>
        </Snackbar>
    )
}