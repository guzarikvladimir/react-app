import React, { useState, useEffect } from 'react'
import {NavLink, useHistory} from 'react-router-dom'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import Hidden from '@material-ui/core/Hidden'
import Drawer from '@material-ui/core/Drawer'
import IconButton from '@material-ui/core/IconButton'
import { makeStyles } from '@material-ui/core'
import classNames from "classnames"
import {connect} from 'react-redux'
import { logOut, getCurrentUser, isAuthorized } from '../../redux/modules/authorization/authActions.js'

import MenuOutlinedIcon from '@material-ui/icons/MenuOutlined'
import FormatListBulletedOutlinedIcon from '@material-ui/icons/FormatListBulletedOutlined'
import EuroOutlinedIcon from '@material-ui/icons/EuroOutlined'
import InfoOutlinedIcon from '@material-ui/icons/InfoOutlined'
import SendOutlinedIcon from '@material-ui/icons/SendOutlined'

import emptyProfileImage from "../../content/assets/empty_profile.png"

import styles from "../../content/styles/navbar/headerStyle.js"
import colors from "../../content/styles/colors.js"
import CustomDropdown from './CustomDropdown.js'

const useStyles = makeStyles(styles)
const useColors = makeStyles(colors)

const Navbar = (props) => {
    const [mobileOpen, setMobileOpen] = useState(false);
    const classes = useStyles()
    const colors = useColors()
    const { color, absolute, fixed, currentUser, dispatch } = props

    const handleDrawerToggle = () => {
        setMobileOpen(!mobileOpen);
    }

    const appBarClasses = classNames({
        [classes.appBar]: true,
        [colors[color]]: color,
        [classes.absolute]: absolute,
        [classes.fixed]: fixed
    })

    useEffect(() => {
      if (isAuthorized() && !currentUser) {
        dispatch(getCurrentUser())
      }
    })

    return (
        <AppBar className={appBarClasses}>
            <Toolbar className={classes.container}>
                <NavLink to="/" exact className={classes.title}>React-app</NavLink>
                <Hidden smDown>
                    <Links {...props} />
                </Hidden>
                <Hidden mdUp>
                    <IconButton
                        color="inherit"
                        aria-label="open drawer"
                        onClick={handleDrawerToggle}
                    >
                        <MenuOutlinedIcon />
                    </IconButton>
                </Hidden>
            </Toolbar>
            <Hidden mdUp>
                <Drawer
                    variant="temporary"
                    anchor={"right"}
                    open={mobileOpen}
                    classes={{paper: classes.drawerPaper}}
                    onClose={handleDrawerToggle}
                >
                    <Links {...props} />
                </Drawer>
            </Hidden>
        </AppBar>
    )
}

const Links = (props) => {
    const classes = useStyles()

    return (
        <List className={classes.list}>
            <Hidden mdUp>
                <ProfileItem {...props}  />
            </Hidden>
            <ListItem className={classes.listItem}>
                <NavLink to="/" exact className={classes.navLink}>
                    <FormatListBulletedOutlinedIcon className={classes.icons} />
                    Notes
                </NavLink>
            </ListItem>
            <ListItem className={classes.listItem}>
                <NavLink to="/exchanger" className={classes.navLink}>
                    <EuroOutlinedIcon className={classes.icons} />
                    Currency exchanger
                </NavLink>
            </ListItem>
            <ListItem className={classes.listItem}>
                <NavLink to="/chat" className={classes.navLink}>
                    <SendOutlinedIcon className={classes.icons} />
                    Chat
                </NavLink>
            </ListItem>
            <ListItem className={classes.listItem}>
                <NavLink to="/about" className={classes.navLink}>
                    <InfoOutlinedIcon className={classes.icons} />
                    About
                </NavLink>
            </ListItem>
            <Hidden smDown>
                <ProfileItem {...props} />
            </Hidden>
        </List>
    )
}

const ProfileItem = (props) => {
  const classes = useStyles()
  const { dispatch, currentUser } = props
  const history = useHistory()

  function handleLogout() {
    dispatch(logOut())
  }

  function handleLogin() {
    const returnUrl = `${window.location.pathname}${window.location.search}`
    history.push(`/login?returnUrl=${encodeURIComponent(returnUrl)}`)
  }

  function handleOnMe() {
    history.push(`/profile/${window.localStorage.userId}`, {
      returnUrl: `${window.location.pathname}${window.location.search}`
    })
  }

  return (
    <ListItem className={classes.listItem}>
      <CustomDropdown
        hoverColor="black"
        buttonText={
          <>
            <img
              src={currentUser?.profileImg ?? emptyProfileImage}
              className={classes.img}
              alt="profile"
            />
            <Hidden mdUp>
              <div className={classes.imageDropdownButtonText}>
                {currentUser?.name}
              </div>
            </Hidden>
          </>
        }
        buttonProps={{className: classes.navLink + " " + classes.imageDropdownButton}}
        dropdownList={isAuthorized() 
          ? [{ text: "Me", onClick: handleOnMe },
            { text: "Log Out", onClick: handleLogout }]
          : [{ text: "Log In", onClick: handleLogin }]
        }
      />
    </ListItem>
  )
}

const mapStateToProperties = (state) => ({
  currentUser: state.auth.currentUser
})

const mapActionsToProperties = (dispatch) => ({
  dispatch
})

export default connect(mapStateToProperties, mapActionsToProperties)(Navbar)