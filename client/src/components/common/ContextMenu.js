import React from 'react';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';

const initialState = {
    mouseX: null,
    mouseY: null
}

const contextId = 'contextMenu'

export const ContextMenu = (props) => {
    const [state, setState] = React.useState(initialState);
    
    const handleClick = (event) => {
        event.preventDefault();
        setState({
            mouseX: event.clientX - 2,
            mouseY: event.clientY - 4,
          });
    };
    
    const handleClose = () => {
        setState(initialState);
        props.onClose()
    };

    const handleSelect = (callback) => {
        callback()
        handleClose()
    }

    return (
        <div onContextMenu={handleClick} style={{ cursor: 'context-menu' }}>
            {props.children}
            <Menu
                keepMounted
                open={state.mouseY !== null}
                onClose={handleClose}
                anchorReference="anchorPosition"
                anchorPosition={
                  state.mouseY !== null && state.mouseX !== null
                    ? { top: state.mouseY, left: state.mouseX }
                    : undefined
                }
                id={contextId}
            >
                {props.menuItems.map((item, index) => (
                    <MenuItem 
                        key={index} 
                        onClick={() => handleSelect(item.onSelect)}
                    >
                        {item.label}
                    </MenuItem>
                ))}
            </Menu>
        </div>
    )
}