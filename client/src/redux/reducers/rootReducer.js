import { combineReducers } from "redux"
import auth from '../modules/authorization/authReducer'
import chat from '../modules/chat/chatReducer'

const rootReducer = combineReducers({
  auth,
  chat
})

export default rootReducer