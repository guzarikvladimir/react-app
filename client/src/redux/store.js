import { createStore } from 'redux'
import { applyMiddleware } from "redux"
import thunk from "redux-thunk"
import rootReducer from "./reducers/rootReducer"

var storeInternal = null

const configureStore = () => {
  storeInternal = createStore(
    rootReducer, 
    applyMiddleware(
      thunk))

  return storeInternal
}

const store = storeInternal ?? configureStore()

export default store