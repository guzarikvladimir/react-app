import { FETCH_USER_DIALOGS, MESSAGE_RECEIVED, DIALOG_RECEIVED, UPDATE_STATUSES, CREATE_DIALOG, SELECTED_DIALOG_CHANGED } from "./chatConstants"
import { LOGOUT } from "../constants"

const handlers = {
  [FETCH_USER_DIALOGS]: (state, payload) => ({ ...state, dialogs: payload.dialogs }),
  [LOGOUT]: (state, payload) => ({ ...state, selectedDialog: null, dialogs: null, selectedDialogUser: null }),
  [MESSAGE_RECEIVED]: (state, payload) => messageReceivedHandler(state, payload.message),
  [DIALOG_RECEIVED]: (state, payload) => dialogReceivedHandler(state, payload.dialog),
  [UPDATE_STATUSES]: (state, payload) => updateStatusesHandler(state, payload.usersStatuses),
  [CREATE_DIALOG]: (state, payload) => createDialogHandler(state, payload.user, payload.dialogId),
  [SELECTED_DIALOG_CHANGED]: (state, payload) => selectedDialogHandler(state, payload.dialog),
  DEFAULT: state => state
}

const initialState = {
  dialogs: null,
  selectedDialog: null,
  selectedDialogUser: null
}

export default (state = initialState, action) => {
  const handler = handlers[action.type] || handlers.DEFAULT

  return handler(state, action.payload)
}

function selectedDialogHandler(state, newDialog) {
  // select local or loaded from the server
  const dialog = state.dialogs.find(d => d.id === newDialog.id)
  const newDelectedDialog = newDialog.messages ? newDialog : dialog
  
  // load user info
  const newSelectedDialogUser = dialog.user
  dialog.unreadMessagesCount = null

  // remove local dialog if it was created
  const newDialogs = Object.assign([], state.dialogs)
  if (state.selectedDialog?.id !== newDelectedDialog?.id && state.selectedDialog?.local) {
    const prevId = newDialogs.findIndex(d => d.id ===state.selectedDialog.id)
    newDialogs.splice(prevId, 1)
  }

  return { ...state, selectedDialog: newDelectedDialog, selectedDialogUser: newSelectedDialogUser, dialogs: newDialogs }
}

function messageReceivedHandler(state, message) {
  let newSelectedDialog = state.selectedDialog
  if (state.selectedDialog) {
    newSelectedDialog = Object.assign({}, state.selectedDialog)
    newSelectedDialog.messages.splice(0, 0, message) 
  }
  
  const newDialogs = Object.assign([], state.dialogs)
  const dialog = newDialogs.find(d => d.id === message.dialogId)
  dialog.lastMessage = message
  // TODO: mark message as unread on the server
  if (!state.selectedDialog || dialog.id !== state.selectedDialog.id) {
    if (!dialog.unreadMessagesCount) {
      dialog.unreadMessagesCount = 1
    } else {
      dialog.unreadMessagesCount++
    }
  }
  
  return { ...state, selectedDialog: newSelectedDialog, dialogs: newDialogs }
}

function dialogReceivedHandler(state, newDialog) {
  const dialogIndex = state.dialogs.findIndex(d => d.id === newDialog.id)
  const newDialogs = Object.assign([], state.dialogs)
  if (dialogIndex >= 0) {
    newDialogs[dialogIndex] = newDialog
  } else {
    newDialogs.splice(0, 0, newDialog)
    newDialog.unreadMessagesCount = 1
  }

  return { ...state, dialogs: newDialogs }
}

function updateStatusesHandler(state, usersStatuses) {
  let newDialogs = Object.assign([], state.dialogs)
  newDialogs = newDialogs.map(dialog => {
    let status = usersStatuses.find(us => us.userId === dialog.user.id)
    if (status.lastSeen) {
      dialog.user.status.lastSeen = status.lastSeen
      dialog.user.status.status = 0
    } else {
      dialog.user.status.status = 1
    }

    return dialog
  })

  return { ...state, dialogs: newDialogs }
}

function createDialogHandler(state, user, dialogId) {
  let dialog = {
    id: dialogId,
    user,
    local: true,
    messages: []
  }
  let newDialogs = Object.assign([], state.dialogs)
  newDialogs.splice(0, 0, dialog)
  
  return { ...state, dialogs: newDialogs }
}