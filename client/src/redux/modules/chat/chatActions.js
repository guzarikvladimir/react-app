import axios from "../../../axios/axios"
import { FETCH_USER_DIALOGS, CREATE_DIALOG, SELECTED_DIALOG_CHANGED } from "./chatConstants"
import { createGuid } from "../../../functions/guid"

export const fetchUserDialogs = () => (dispatch) => {
  return axios.get(`${process.env.REACT_APP_CHAT_URL}/api/dialogs/user/${window.localStorage.userId}`)
    .then(response => {
      if (response && response.status === 200) {
        dispatch({
          type: FETCH_USER_DIALOGS,
          payload: {
            dialogs: response.data
          }
        })
      }

      return response.data
    })
}

export const getDialog = (dialogId) => (dispatch) => {
  return axios.get(`${process.env.REACT_APP_CHAT_URL}/api/dialogs/${dialogId}`)
    .then(response => {
      if (response && response.status === 200) {
        dispatch({
          type: SELECTED_DIALOG_CHANGED,
          payload: {
            dialog: response.data,
          }
        })
      }
    })
}

export const sendMessage = (text, dialogId, userId) => (dispatch) => {
  return axios.post(`${process.env.REACT_APP_CHAT_URL}/api/messages`,
    {
      dialogId: dialogId,
      usersIds: [window.localStorage.userId, userId],
      message: {
        text,
        createdDate: new Date()
      }
    }).then(({data}) => {
      /*dispatch({
        type: MESSAGE_SENT,
        payload: {
          
        }
      })*/

      return new Promise((resolve) => {
        resolve()
      })
    })
}

export const searchContact = (name) => {
  return axios.post(`${process.env.REACT_APP_CHAT_URL}/api/users`, {
    userId: window.localStorage.userId,
    name: name
  })
}

export const createDialog = (user) => (dispatch) => {
  let newId = createGuid()
  dispatch({
    type: CREATE_DIALOG,
    payload: {
      user,
      dialogId: newId
    }
  })

  return newId
}

export const selectDialog = dialogId => dispatch => {
  dispatch({
    type: SELECTED_DIALOG_CHANGED,
    payload: {
      dialog: {
        id: dialogId
      }
    }
  })
}

export const getUserInfo = userId => {
  return axios.get(`${process.env.REACT_APP_CHAT_URL}/api/users/info/${userId}`)
    .then(response => {
      if (response && response.status === 200) {
        return response.data
      }
    })
}