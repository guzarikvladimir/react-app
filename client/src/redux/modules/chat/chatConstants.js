export const FETCH_USER_DIALOGS = "FETCH_USER_DIALOGS"
export const SELECTED_DIALOG_CHANGED = "SELECTED_DIALOG_CHANGED"
export const MESSAGE_SENT = "MESSAGE_SENT"

export const MESSAGE_RECEIVED = "MESSAGE_RECEIVED"
export const DIALOG_RECEIVED = "DIALOG_RECEIVED"
export const UPDATE_STATUSES = "UPDATE_STATUSES"

export const CREATE_DIALOG = "CREATE_DIALOG"