import { SET_CURRENT_USER } from "./authConstants"
import { LOGOUT } from "../constants"
import axios from "../../../axios/axios"
import { connectToChat, disconnectFromChat } from "../../../sockets/connection"

export const logIn = (email, password) => dispatch => {
  return axios.post(`${process.env.REACT_APP_AUTHORITY_URL}/api/auth/token`,{
    email: email,
    password: password
  }).then(response => {
    if (response && response.status === 200) {
      setData(response.data)
      connectToChat(dispatch)

      return getCurrentUserInternal(dispatch)
    }
  })
}

export const register = model => dispatch => {
  return axios.post(`${process.env.REACT_APP_AUTHORITY_URL}/api/account`, {
    ...model
  })
}

export const getCurrentUser = () => dispatch => {
  return getCurrentUserInternal(dispatch)
}

const getCurrentUserInternal = dispatch => {
  return axios.get(`${process.env.REACT_APP_CHAT_URL}/api/users/info/${window.localStorage.userId}`)
    .then(response => {
      if (response && response.status === 200) {
        dispatch({
          type: SET_CURRENT_USER, 
          payload: {
            user: response.data
          }
        })
      }
    })
}

export const isAuthorized = () => {
  return !!window.localStorage.refreshToken && !!window.localStorage.userId
}

const requestState = {
  authTokenRequest: null
}

export const refreshToken = () => {
  if (!requestState.authTokenRequest) {
    requestState.authTokenRequest = axios.post(`${process.env.REACT_APP_AUTHORITY_URL}/api/auth/refresh`, {
      userId: window.localStorage.userId,
      refreshToken: window.localStorage.refreshToken
    }).then(response => {
      if (response && response.status === 200) {
        setData(response.data)
      }
    }).then(resetAuthTokenRequest, error => {
      clearTokens()
      resetAuthTokenRequest()
    })
  }

  return requestState.authTokenRequest
}

function resetAuthTokenRequest() {
  requestState.authTokenRequest = null;
}

function setData(data)
{
  window.localStorage["token"] = 'Bearer ' + data.token
  window.localStorage["refreshToken"] = data.refreshToken
  // TODO: check if can be obtained stored on the server 
  window.localStorage["userId"] = data.userId
}

export const logOut = () => dispatch => {
  clearTokens()
  disconnectFromChat()

  dispatch({
    type: LOGOUT
  })
}

export const clearTokens = () => {
  delete window.localStorage.token
  delete window.localStorage.refreshToken
  delete window.localStorage.userId
}