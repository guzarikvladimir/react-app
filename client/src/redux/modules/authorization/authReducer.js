import { SET_CURRENT_USER } from "./authConstants"
import { LOGOUT } from "../constants"

const handlers = {
  [SET_CURRENT_USER]: (state, payload) => ({ ...state, currentUser: payload.user }),
  [LOGOUT]: (state, payload) => ({ ...state, currentUser: null }),
  DEFAULT: state => state
}

const initialState = {
  currentUser: null
}

export default (state = initialState, action) => {
  const handler = handlers[action.type] || handlers.DEFAULT

  return handler(state, action.payload)
}