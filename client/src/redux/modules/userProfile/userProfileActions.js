import axios from "../../../axios/axios"

export const updateUserProfile = (userData) => {
  return axios.post(`${process.env.REACT_APP_CHAT_URL}/api/users/update`, {
    ...userData
  })
}