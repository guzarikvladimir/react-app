import axios from 'axios'
import { refreshToken, isAuthorized } from '../redux/modules/authorization/authActions';

axios.interceptors.request.use(function (config) {
  // Do something before request is sent
  if (config.url.startsWith(process.env.REACT_APP_AUTHORITY_URL)
    || config.url.startsWith(process.env.REACT_APP_CHAT_URL)) {
    config.headers.common["Authorization"] = window.localStorage.token
  } else {
    delete config.headers.common["Authorization"]
  }

  return config
}, function (error) {
  // Do something with request error
  return Promise.reject(error)
});

axios.interceptors.response.use(
  response => response,
  error => {
    if (error.response && error.response.status === 401) {
      return handler401(error)
    } else {
      return Promise.reject(error)
    }
  }
)

function handler401(error) {
  if (isAuthorized()) {
    return refreshToken().then(() => {
      error.config.headers["Authorization"] = window.localStorage.token
      return axios(error.config)
    })
  }

  return Promise.reject(error)
}

export default axios