import { MESSAGE_RECEIVED, UPDATE_STATUSES, DIALOG_RECEIVED } from "../redux/modules/chat/chatConstants"
import { HubConnectionBuilder } from '@microsoft/signalr'

const connectionState = {
  connection: null
}

const connectToChat = dispatch => {
  connectionState.connection = new HubConnectionBuilder()
    .withUrl(`${process.env.REACT_APP_CHAT_URL}/chatHub?userId=${window.localStorage.userId}`)
    .withAutomaticReconnect()
    .build()
  connectionState.connection.start().then(() => console.log('connection started'))
    .catch(err => console.log('connection error', err))
  connectionState.connection.on('receiveMessage', message => {
    dispatch({
      type: MESSAGE_RECEIVED,
      payload: {
        message
      }
    })
  })
  connectionState.connection.on('receiveDialog', dialog => {
    dispatch({
      type: DIALOG_RECEIVED,
      payload: {
        dialog
      }
    })
  })
  connectionState.connection.on('updateStatuses', usersStatuses => {
    dispatch({
      type: UPDATE_STATUSES,
      payload: {
        usersStatuses
      }
    })
  })
}

const disconnectFromChat = () => {
  connectionState.connection.stop().then(() => {
    connectionState.connection = null
  })
}


export {
  connectToChat,
  disconnectFromChat
}