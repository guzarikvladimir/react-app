import React from 'react'
import { Route, Switch } from "react-router-dom";
import withAuthorization from "./hooks/withAuthorization";
import withAnonymOnly from "./hooks/withAnonymOnly";
import Chat from "./pages/Chat";
import Login from './pages/Login';
import Register from './pages/Register';
import { Home } from './pages/Home';
import { Exchanger } from './pages/Exchanger';
import { About } from './pages/About';
import UserProfile from './pages/UserProfile';
import { Alert } from './components/common/Alert';

export const Routes = () => {
  return (
    <>
    <Alert />
    <Switch>
      <Route path={'/'} exact component={Home}/>
      <Route path={'/exchanger'} exact component={Exchanger}/>
      <Route path={'/about'} exact component={About}/>
      <Route path={'/chat'} exact component={withAuthorization(Chat)}/>
      <Route path={'/chat/:dialogId'} exact component={withAuthorization(Chat)}/>
      <Route path={'/login'} exact component={withAnonymOnly(Login)}/>
      <Route path={'/register'} exact component={withAnonymOnly(Register)}/>
      <Route path={'*'} component={Home}/>
    </Switch>
    <Route path={'/profile/:userId'} exact component={UserProfile}/>
    </>
  )
}