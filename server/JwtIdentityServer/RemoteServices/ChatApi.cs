﻿using System.Net.Http;
using System.Threading.Tasks;
using JwtIdentityServer.RemoteServices.Contract;
using JwtIdentityServer.RemoteServices.Models;
using Microsoft.AspNetCore.Http;

namespace JwtIdentityServer.RemoteServices
{
    public class ChatApi : RemoteApiBase, IChatApi
    {
        public ChatApi(
            HttpClient httpClient, 
            IHttpContextAccessor httpContextAccessor, 
            bool setAuthorizationHeader = false) 
            : base(httpClient, httpContextAccessor, setAuthorizationHeader)
        {
        }

        public async Task<HttpResponseMessage> CreateUserProfile(UserProfileModel model)
        {
            string url = "api/users/create";
            StringContent data = SerializeData(model);
            HttpResponseMessage response = await HttpClient.PostAsync(url, data);

            return response;
        }
    }
}