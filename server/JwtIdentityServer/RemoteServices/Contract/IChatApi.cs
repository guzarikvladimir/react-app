﻿using System.Net.Http;
using System.Threading.Tasks;
using JwtIdentityServer.RemoteServices.Models;

namespace JwtIdentityServer.RemoteServices.Contract
{
    public interface IChatApi
    {
        Task<HttpResponseMessage> CreateUserProfile(UserProfileModel model);
    }
}