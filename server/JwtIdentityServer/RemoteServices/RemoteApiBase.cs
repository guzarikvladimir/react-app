﻿using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;

namespace JwtIdentityServer.RemoteServices
{
    public abstract class RemoteApiBase
    {
        protected readonly HttpClient HttpClient;
        protected readonly IHttpContextAccessor HttpContextAccessor;

        protected RemoteApiBase(
            HttpClient httpClient, 
            IHttpContextAccessor httpContextAccessor, 
            bool setAuthorizationHeader = false)
        {
            HttpClient = httpClient;
            HttpContextAccessor = httpContextAccessor;

            if (setAuthorizationHeader)
            {
                TrySetAuthorizationHeader();
            }
        }

        protected async Task<TResponse> MakeGetRequest<TResponse>(string url)
        {
            HttpResponseMessage response = await HttpClient.GetAsync(url);
            string rawData = await response.Content.ReadAsStringAsync();

            TResponse data = JsonConvert.DeserializeObject<TResponse>(rawData);

            return data;
        }

        protected StringContent SerializeData(object obj)
        {
            return new StringContent(
                JsonConvert.SerializeObject(obj),
                Encoding.UTF8,
                "application/json");
        }

        protected bool TrySetAuthorizationHeader()
        {
            bool isSuccessful = false;
            string authorizationHeader = HttpContextAccessor
                .HttpContext
                .Request
                .Headers["Authorization"]
                .FirstOrDefault();

            if (IsAuthorizationHeaderValid(authorizationHeader))
            {
                HttpClient.DefaultRequestHeaders.Add("Authorization", authorizationHeader);
                isSuccessful = true;
            }

            return isSuccessful;
        }

        private bool IsAuthorizationHeaderValid(string authorizationHeader)
        {
            string[] splittedAuthorizationHeader = authorizationHeader?.Split(' ');
            bool isHeaderValid = splittedAuthorizationHeader != null
                                 && splittedAuthorizationHeader.Length == 2
                                 && splittedAuthorizationHeader[0] == "Bearer";

            return isHeaderValid;
        }
    }
}