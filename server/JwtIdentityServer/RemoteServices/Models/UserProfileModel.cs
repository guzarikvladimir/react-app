﻿using System;

namespace JwtIdentityServer.RemoteServices.Models
{
    public class UserProfileModel
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public string Email { get; set; }

        public string? PhoneNumber { get; set; }

        public string? ProfileImg { get; set; }
    }
}