﻿using JwtIdentityServer.Infrastructure.EF;
using JwtIdentityServer.Infrastructure.EF.Contract;
using JwtIdentityServer.Infrastructure.EF.Repositories;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace JwtIdentityServer.Extensions
{
    public static class DatabaseServicesExtension
    {
        public static void AddRepositories(this IServiceCollection services, string connectionString)
        {
            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseSqlServer(connectionString));

            services.AddScoped<IRefreshTokenRepository, RefreshTokenRepository>();
        }
    }
}