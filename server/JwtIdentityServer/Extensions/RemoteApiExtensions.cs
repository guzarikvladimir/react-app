﻿using System;
using System.Net.Http.Headers;
using JwtIdentityServer.Models.External;
using JwtIdentityServer.RemoteServices;
using JwtIdentityServer.RemoteServices.Contract;
using Microsoft.Extensions.DependencyInjection;

namespace JwtIdentityServer.Extensions
{
    public static class RemoteApiExtensions
    {
        public static void AddHttp(this IServiceCollection services, ExternalApiOptionsContainer externalApis)
        {
            services.AddHttpClient<IChatApi, ChatApi>(client =>
            {
                client.BaseAddress = new Uri(externalApis.Chat.Uri);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            });
        }
    }
}