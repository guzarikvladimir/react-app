﻿using JwtIdentityServer.Infrastructure.EF.Contract;
using JwtIdentityServer.Models.Identity;

namespace JwtIdentityServer.Infrastructure.EF.Repositories
{
    public class RefreshTokenRepository : 
        EFRepository<RefreshToken>, 
        IRefreshTokenRepository
    {
        public RefreshTokenRepository(ApplicationDbContext context) 
            : base(context)
        {
        }
    }
}