﻿using JwtIdentityServer.Models.Identity;

namespace JwtIdentityServer.Infrastructure.EF.Contract
{
    public interface IRefreshTokenRepository : IRepository<RefreshToken>
    {
    }
}