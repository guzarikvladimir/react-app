﻿using System;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace JwtIdentityServer.Infrastructure.EF.Contract
{
    public interface IRepository<T>
    {
        Task<T> FindFirstAsync(Expression<Func<T, bool>> predicate);

        Task AddAsync(T entity);

        Task RemoveAsync(T entity);
    }
}