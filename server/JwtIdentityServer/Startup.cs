using System;
using System.Net.Http.Headers;
using System.Text;
using AutoMapper;
using JwtIdentityServer.Extensions;
using JwtIdentityServer.Infrastructure.EF;
using JwtIdentityServer.Jwt;
using JwtIdentityServer.Jwt.Models;
using JwtIdentityServer.Models.External;
using JwtIdentityServer.Models.Identity;
using JwtIdentityServer.RemoteServices;
using JwtIdentityServer.RemoteServices.Contract;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;

namespace JwtIdentityServer
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddRepositories(Configuration.GetConnectionString("DefaultConnection"));
            
            services.AddAuth(Configuration);

            services.AddControllers();

            ExternalApiOptionsContainer externalApis = Configuration
                .GetSection("ExternalApis")
                .Get<ExternalApiOptionsContainer>(x => x.BindNonPublicProperties = true);
            services.AddCors(setup =>
            {
                setup.AddDefaultPolicy(policy =>
                {
                    policy.AllowAnyHeader();
                    policy.AllowAnyMethod();
                    policy.WithOrigins(externalApis.Client.Uri);
                    policy.AllowCredentials();

                });
            });
            
            services.AddHttp(externalApis);
            services.AddAutoMapper(typeof(Startup));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseCors();
            app.UseRouting();
            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
