﻿using AutoMapper;
using JwtIdentityServer.Models;
using JwtIdentityServer.Models.Identity;
using JwtIdentityServer.RemoteServices.Models;

namespace JwtIdentityServer.Mapper
{
    public class UserProfile : Profile
    {
        public UserProfile()
        {
            CreateMap<RegistrationModel, User>()
                .ForMember(dst => dst.UserName, opt => opt.MapFrom(src => src.Email));

            CreateMap<RegistrationModel, UserProfileModel>();
        }
    }
}