﻿using System;
using System.Diagnostics.Eventing.Reader;
using System.Net;
using System.Threading.Tasks;
using AutoMapper;
using JwtIdentityServer.Constants;
using JwtIdentityServer.Helpers;
using JwtIdentityServer.Models;
using JwtIdentityServer.Models.Exceptions;
using JwtIdentityServer.Models.Identity;
using JwtIdentityServer.RemoteServices.Contract;
using JwtIdentityServer.RemoteServices.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace JwtIdentityServer.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class AccountController : Controller
    {
        private readonly UserManager<User> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly IMapper _mapper;
        private readonly IChatApi _chatApi;

        public AccountController(
            UserManager<User> userManager,
            RoleManager<IdentityRole> roleManager,
            IMapper mapper,
            IChatApi chatApi)
        {
            _userManager = userManager;
            _roleManager = roleManager;
            _mapper = mapper;
            _chatApi = chatApi;
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] RegistrationModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            bool isValid = await Validate(model);
            if (!isValid)
            {
                return BadRequest(ModelState);
            }

            var userIdentity = _mapper.Map<User>(model);
            var userResult = await _userManager.CreateAsync(userIdentity, model.Password);
            if (!userResult.Succeeded)
            {
                return BadRequest(Errors.AddErrorsToModelState(userResult, ModelState));
            }

            string role = Roles.User;
            if (await _roleManager.FindByNameAsync(role) == null)
            {
                IdentityResult roleResult = await _roleManager.CreateAsync(new IdentityRole(role));
                if (roleResult.Succeeded)
                {
                    await _userManager.AddToRoleAsync(userIdentity, role);
                }
            }

            var profileModel = _mapper.Map<UserProfileModel>(model);
            profileModel.Id = Guid.Parse(userIdentity.Id);
            var response = await _chatApi.CreateUserProfile(profileModel);

            return response.IsSuccessStatusCode ? (IActionResult) Ok(response) : BadRequest(response);
        }

        [HttpDelete]
        public async Task<IActionResult> Delete([FromQuery] Guid userId)
        {
            User user = await _userManager.FindByIdAsync(userId.ToString());
            if (user == null)
            {
                return BadRequest(Errors.AddErrorToModelState(
                    HttpStatusCode.NotFound.ToString(), 
                    "User does not exist",
                    ModelState));
            }

            await _userManager.DeleteAsync(user);

            return Ok("User deleted");
        }

        public async Task<bool> Validate(RegistrationModel model)
        {
            User userExists = await _userManager.FindByEmailAsync(model.Email);
            if (userExists != null)
            {
                BadRequest(Errors.AddErrorToModelState(nameof(model.Email), "User already exists", ModelState));

                return false;
            }

            return true;
        }
    }
}