﻿using System.Threading.Tasks;
using JwtIdentityServer.Helpers;
using JwtIdentityServer.Infrastructure.EF.Contract;
using JwtIdentityServer.Jwt;
using JwtIdentityServer.Models;
using JwtIdentityServer.Models.Identity;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace JwtIdentityServer.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class AuthController : Controller
    {
        private readonly UserManager<User> _userManager;
        private readonly IJwtFactory _jwtFactory;
        private readonly IRefreshTokenRepository _refreshTokenRepository;

        public AuthController(
            UserManager<User> userManager, 
            IJwtFactory jwtFactory,
            IRefreshTokenRepository refreshTokenRepository)
        {
            _userManager = userManager;
            _jwtFactory = jwtFactory;
            _refreshTokenRepository = refreshTokenRepository;
        }

        [HttpPost("token")]
        public async Task<IActionResult> Post([FromBody] LoginModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var userToVerify = await _userManager.FindByNameAsync(model.Email);
            if (userToVerify == null || !await _userManager.CheckPasswordAsync(userToVerify, model.Password))
            {
                BadRequest(Errors.AddErrorToModelState("email", "Invalid username or password.", ModelState));
                return BadRequest(ModelState);
            }

            TokensViewModel tokens = await GetTokens(userToVerify);

            return Ok(tokens);
        }

        [HttpPost]
        [Route("refresh")]
        public async Task<IActionResult> RefreshToken([FromBody] RefreshTokenModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            User user = await _userManager.FindByIdAsync(model.UserId);
            if (user == null)
            {
                return BadRequest("Invalid user");
            }

            RefreshToken existingToken = await _refreshTokenRepository
                .FindFirstAsync(rf => rf.UserId == model.UserId && rf.Value == model.RefreshToken);
            if (existingToken == null)
            {
                return BadRequest("Invalid refresh token");
            }

            await _refreshTokenRepository.RemoveAsync(existingToken);

            TokensViewModel tokens = await GetTokens(user);

            return Ok(tokens);
        }

        [NonAction]
        public async Task<TokensViewModel> GetTokens(User user)
        {
            string token = _jwtFactory.GenerateToken(user);
            string refreshToken = _jwtFactory.GenerateRefreshToken();
            await _refreshTokenRepository.AddAsync(new RefreshToken()
            {
                Value = refreshToken,
                UserId = user.Id
            });

            return new TokensViewModel()
            {
                UserId = user.Id,
                Token = token,
                RefreshToken = refreshToken
            };
        }
    }
}