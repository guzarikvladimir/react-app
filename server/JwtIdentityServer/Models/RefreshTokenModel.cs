﻿using System.ComponentModel.DataAnnotations;

namespace JwtIdentityServer.Models
{
    public class RefreshTokenModel
    {
        [Required]
        public string UserId { get; set; }

        [Required]
        public string RefreshToken { get; set; }
    }
}