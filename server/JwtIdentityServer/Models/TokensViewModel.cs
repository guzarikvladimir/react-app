﻿namespace JwtIdentityServer.Models
{
    public class TokensViewModel
    {
        public string UserId { get; set; }

        public string Token { get; set; }

        public string RefreshToken { get; set; }
    }
}