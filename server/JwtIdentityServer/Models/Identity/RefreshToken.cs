﻿using System;

namespace JwtIdentityServer.Models.Identity
{
    public class RefreshToken
    {
        public Guid Id { get; set; }

        public string UserId { get; set; }

        public string Value { get; set; }
    }
}