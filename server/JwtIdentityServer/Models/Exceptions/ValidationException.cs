﻿using System;

namespace JwtIdentityServer.Models.Exceptions
{
    public class ValidationException : Exception
    {
        public string FieldName { get; }

        public ValidationException(string message) : base(message)
        {
        }

        public ValidationException(string message, string fieldName) : base(message)
        {
            FieldName = fieldName;
        }
    }
}