﻿using JwtIdentityServer.Models.Configuration;

namespace JwtIdentityServer.Models.External
{
    public class ExternalApiOptionsContainer
    {
        public ExternalApiOptions Chat { get; set; }

        public ExternalApiOptions Client { get; set; }
    }
}