﻿namespace JwtIdentityServer.Models.Configuration
{
    public class ExternalApiOptions
    {
        public string Uri { get; private set; }
    }
}