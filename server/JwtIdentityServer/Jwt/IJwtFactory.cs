﻿using JwtIdentityServer.Models.Identity;

namespace JwtIdentityServer.Jwt
{
    public interface IJwtFactory
    {
        string GenerateToken(User user);

        string GenerateRefreshToken(int length = 60);
    }
}