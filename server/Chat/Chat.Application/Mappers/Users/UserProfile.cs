﻿using System.Security;
using AutoMapper;
using Chat.Application.Models;
using Chat.Domain.Clients.Models;
using Chat.Domain.Models;

namespace Chat.Application.Mappers.Users
{
    public class UserProfile : Profile
    {
        public UserProfile()
        {
            CreateMap<User, UserViewModel>();

            CreateMap<UserModel, User>();

            CreateMap<User, UserShortViewModel>();

            CreateMap<UserStatus, UserStatusViewModel>();
        }
    }
}