﻿using AutoMapper;
using Chat.Application.Models;
using Chat.Domain.Clients.Models;
using Chat.Domain.Models;

namespace Chat.Application.Mappers.Messages
{
    public class MessagesProfile : Profile
    {
        public MessagesProfile()
        {
            CreateMap<MessageModel, Message>();

            CreateMap<Message, MessageViewModel>();

            CreateMap<Message, MessageClientViewModel>();
        }
    }
}