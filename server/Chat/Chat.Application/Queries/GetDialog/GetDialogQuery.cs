﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Chat.Application.Models;
using Chat.Domain.Clients.Models;
using Chat.Domain.Models;
using Chat.Domain.Repositories;
using Microsoft.EntityFrameworkCore;

namespace Chat.Application.Queries.GetDialog
{
    public class GetDialogQuery : IGetDialogQuery
    {
        private readonly IQueryService _queryService;
        private readonly IMapper _mapper;

        public GetDialogQuery(
            IQueryService queryService, 
            IMapper mapper)
        {
            _queryService = queryService;
            _mapper = mapper;
        }

        public async Task<DialogViewModel> ExecuteAsync(Guid dialogId)
        {
            Dialog dialog = await _queryService.Dialogs
                .Include(d => d.Messages)
                .FirstOrDefaultAsync(d => d.Id == dialogId);

            List<MessageViewModel> messages = _mapper
                .Map<IEnumerable<MessageViewModel>>(dialog.Messages)
                .OrderByDescending(m => m.CreatedDate)
                .ToList();

            return new DialogViewModel()
            {
                Id = dialog.Id,
                Messages = messages
            };
        }
    }
}