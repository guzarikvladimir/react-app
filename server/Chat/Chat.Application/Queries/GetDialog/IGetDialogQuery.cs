﻿using System;
using Chat.Application.Models;

namespace Chat.Application.Queries.GetDialog
{
    public interface IGetDialogQuery : IQuery<Guid, DialogViewModel>
    {
    }
}