﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Chat.Domain.Clients.Models;
using Chat.Domain.Models;
using Chat.Domain.Repositories;
using Microsoft.EntityFrameworkCore;

namespace Chat.Application.Queries.GetUserDialogs
{
    public class GetUserDialogsQuery : IGetUserDialogsQuery
    {
        private readonly IQueryService _queryService;
        private readonly IMapper _mapper;

        public GetUserDialogsQuery(
            IQueryService queryService,
            IMapper mapper)
        {
            _queryService = queryService;
            _mapper = mapper;
        }

        public async Task<IEnumerable<DialogPreviewViewModel>> ExecuteAsync(Guid userId)
        {
            List<Guid> dialogsIds = await _queryService.UsersDialogs
                .Where(ud => ud.UserId == userId)
                .Select(ud => ud.DialogId)
                .ToListAsync();

            List<DialogPreviewViewModel> result = new List<DialogPreviewViewModel>();
            foreach (Guid dialogId in dialogsIds)
            {
                UserDialog userDialog = await _queryService.UsersDialogs
                    .Include(ud => ud.User.Status)
                    .FirstOrDefaultAsync(ud => ud.DialogId == dialogId && ud.UserId != userId);
                
                Message lastMessage = await _queryService.Messages
                    .OrderBy(m => m.CreatedDate)
                    .LastOrDefaultAsync(m => m.DialogId == dialogId);

                result.Add(new DialogPreviewViewModel()
                {
                    Id = dialogId,
                    User = _mapper.Map<UserShortViewModel>(userDialog.User),
                    LastMessage = _mapper.Map<MessageViewModel>(lastMessage),
                });
            }

            return result;
        }
    }
}