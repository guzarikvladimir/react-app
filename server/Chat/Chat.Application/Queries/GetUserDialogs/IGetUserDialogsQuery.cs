﻿using System;
using System.Collections.Generic;
using Chat.Application.Models;
using Chat.Domain.Clients.Models;

namespace Chat.Application.Queries.GetUserDialogs
{
    public interface IGetUserDialogsQuery : IQuery<Guid, IEnumerable<DialogPreviewViewModel>>
    {
    }
}