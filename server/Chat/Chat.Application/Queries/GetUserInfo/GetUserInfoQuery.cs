﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Chat.Application.Models;
using Chat.Domain.Models;
using Chat.Domain.Repositories;
using Microsoft.EntityFrameworkCore;

namespace Chat.Application.Queries.GetUserInfo
{
    public class GetUserInfoQuery : IGetUserInfoQuery, IGetUsersInfoQuery
    {
        private readonly IQueryService _queryService;
        private readonly IMapper _mapper;

        public GetUserInfoQuery(IQueryService queryService, IMapper mapper)
        {
            _queryService = queryService;
            _mapper = mapper;
        }

        public async Task<UserViewModel> ExecuteAsync(Guid inputModel)
        {
            User user = await _queryService.Users.FirstOrDefaultAsync(u => u.Id == inputModel);
            if (user == null)
            {
                throw new Exception("User does not exist");
            }

            return _mapper.Map<UserViewModel>(user);
        }

        public async Task<List<UserViewModel>> ExecuteAsync(List<Guid> inputModel)
        {
            List<UserViewModel> users = await _queryService.Users
                .Where(u => inputModel.Contains(u.Id))
                .Select(u => _mapper.Map<UserViewModel>(u))
                .ToListAsync();

            return users;
        }
    }
}