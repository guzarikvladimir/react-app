﻿using System;
using System.Collections.Generic;
using Chat.Application.Models;

namespace Chat.Application.Queries.GetUserInfo
{
    public interface IGetUsersInfoQuery : IQuery<List<Guid>, List<UserViewModel>>
    {
    }
}