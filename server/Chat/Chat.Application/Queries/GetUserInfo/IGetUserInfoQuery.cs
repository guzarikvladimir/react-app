﻿using System;
using Chat.Application.Models;

namespace Chat.Application.Queries.GetUserInfo
{
    public interface IGetUserInfoQuery : IQuery<Guid, UserViewModel>
    {
    }
}