﻿using System.Threading.Tasks;

namespace Chat.Application.Queries
{
    public interface IQuery<TOutput>
    {
        Task<TOutput> ExecuteAsync();
    }

    public interface IQuery<TInput, TOutput>
    {
        Task<TOutput> ExecuteAsync(TInput inputModel);
    }
}