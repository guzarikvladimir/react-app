﻿using System.Collections.Generic;
using Chat.Application.Models;

namespace Chat.Application.Queries.GetContacts
{
    public interface IGetContactsQuery : IQuery<GetContactsModel, IEnumerable<UserViewModel>>
    {
    }
}