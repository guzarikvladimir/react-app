﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Chat.Application.Models;
using Chat.Domain.Models;
using Chat.Domain.Repositories;
using Microsoft.EntityFrameworkCore;

namespace Chat.Application.Queries.GetContacts
{
    public class GetContactsQuery : IGetContactsQuery
    {
        private readonly IQueryService _queryService;
        private readonly IMapper _mapper;

        public GetContactsQuery(IQueryService queryService, IMapper mapper)
        {
            _queryService = queryService;
            _mapper = mapper;
        }

        public async Task<IEnumerable<UserViewModel>> ExecuteAsync(GetContactsModel model)
        {
            if (string.IsNullOrEmpty(model.Name))
            {
                return Enumerable.Empty<UserViewModel>();
            }

            List<User> users = await _queryService.Users
                .Where(u => (u.Name.ToUpper().Contains(model.Name.ToUpper()) 
                             || u.Email.ToUpper().Contains(model.Name.ToUpper())) 
                            && u.Id != model.UserId)
                .ToListAsync();

            return users.Select(u => _mapper.Map<UserViewModel>(u));
        }
    }
}