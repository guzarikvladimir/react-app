﻿using System;

namespace Chat.Application.Queries.GetContacts
{
    public class GetContactsModel
    {
        public Guid UserId { get; set; }

        public string Name { get; set; }
    }
}