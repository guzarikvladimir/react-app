﻿using System;

namespace Chat.Application.Models
{
    public class UserStatusModel
    {
        public Guid UserId { get; set; }

        public DateTime LastSeen { get; set; }
    }
}