﻿using System;

namespace Chat.Application.Models
{
    public class MessageModel
    {
        public Guid? Id { get; set; }

        public Guid? DialogId { get; set; }

        public string Text { get; set; }

        public DateTime CreatedDate { get; set; }
    }
}