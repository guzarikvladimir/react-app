﻿using System;
using System.Collections.Generic;
using Chat.Domain.Clients.Models;

namespace Chat.Application.Models
{
    public class DialogViewModel
    {
        public Guid Id { get; set; }

        public IEnumerable<MessageViewModel> Messages { get; set; }
    }
}