﻿using System.Threading.Tasks;
using AutoMapper;
using Chat.Application.Models;
using Chat.Domain.Models;
using Chat.Domain.Repositories;

namespace Chat.Application.Commands.UpdateUser
{
    public class UpdateUserCommand: IUpdateUserCommand
    {
        private readonly IUserRepository _userRepository;
        private readonly IMapper _mapper;

        public UpdateUserCommand(
            IUserRepository userRepository,
            IMapper mapper)
        {
            _userRepository = userRepository;
            _mapper = mapper;
        }

        public async Task ExecuteAsync(UserModel model)
        {
            var user = await _userRepository.FindAsync(model.Id);
            user.ProfileImg = model.ProfileImg;
            await _userRepository.UpdateAsync(user);
        }
    }
}