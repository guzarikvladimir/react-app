﻿using Chat.Application.Models;

namespace Chat.Application.Commands.UpdateUser
{
    public interface IUpdateUserCommand : ICommand<UserModel>
    {
    }
}