﻿using System.Threading.Tasks;
using AutoMapper;
using Chat.Application.Models;
using Chat.Domain.Models;
using Chat.Domain.Repositories;
using Chat.Domain.Specifications.Impl;

namespace Chat.Application.Commands.CreateUser
{
    public class CreateUserCommand : ICreateUserCommand
    {
        private readonly IMapper _mapper;
        private readonly IUserRepository _userRepository;

        public CreateUserCommand(
            IMapper mapper,
            IUserRepository userRepository)
        {
            _mapper = mapper;
            _userRepository = userRepository;
        }

        public async Task ExecuteAsync(UserModel model)
        {
            var userByName = new GetUserByNameSpecification(model.Email);
            User userExists = await _userRepository.FindAsync(userByName);
            if (userExists == null)
            {
                User user = _mapper.Map<User>(model);
                await _userRepository.CreateAsync(user);
            }
        }
    }
}