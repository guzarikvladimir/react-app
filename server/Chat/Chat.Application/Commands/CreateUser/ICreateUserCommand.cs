﻿using Chat.Application.Models;

namespace Chat.Application.Commands.CreateUser
{
    public interface ICreateUserCommand : ICommand<UserModel>
    {
    }
}