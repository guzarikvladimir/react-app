﻿using System.Threading.Tasks;
using Chat.Application.Models;
using Chat.Domain.Models;
using Chat.Domain.Repositories;

namespace Chat.Application.Commands.UpdateUserStatus
{
    public class UpdateUserStatusCommand : IUpdateUserStatusCommand
    {
        private readonly IUserStatusRepository _userStatusRepository;

        public UpdateUserStatusCommand(IUserStatusRepository userStatusRepository)
        {
            _userStatusRepository = userStatusRepository;
        }

        public async Task ExecuteAsync(UserStatusModel model)
        {
            UserStatus userStatus = await _userStatusRepository.FindAsync(model.UserId);
            userStatus.LastSeen = userStatus.LastSeen;
            await _userStatusRepository.UpdateAsync(userStatus);
        }
    }
}