﻿using Chat.Application.Models;

namespace Chat.Application.Commands.UpdateUserStatus
{
    public interface IUpdateUserStatusCommand : ICommand<UserStatusModel>
    {
    }
}