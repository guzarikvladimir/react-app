﻿using System;

namespace Chat.Application.Commands.SendMessage
{
    public interface ISendMessageCommand : ICommand<SendMessageModel, Guid>
    {
    }
}