﻿using System;
using System.Collections.Generic;
using Chat.Application.Models;

namespace Chat.Application.Commands.SendMessage
{
    public class SendMessageModel
    {
        public Guid DialogId { get; set; }

        public List<Guid> UsersIds { get; set; }

        public MessageModel Message { get; set; }
    }
}