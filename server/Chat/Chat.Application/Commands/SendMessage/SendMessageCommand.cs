﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Chat.Domain.Clients.Models;
using Chat.Domain.Clients.Services;
using Chat.Domain.Models;
using Chat.Domain.Repositories;
using Microsoft.EntityFrameworkCore;

namespace Chat.Application.Commands.SendMessage
{
    public class SendMessageCommand : ISendMessageCommand
    {
        private readonly IDialogRepository _dialogRepository;
        private readonly IMapper _mapper;
        private readonly IDialogMessageSender _dialogMessageSender;
        private readonly IQueryService _queryService;

        public SendMessageCommand(
            IDialogRepository dialogRepository,
            IMapper mapper,
            IDialogMessageSender dialogMessageSender,
            IQueryService queryService)
        {
            _dialogRepository = dialogRepository;
            _mapper = mapper;
            _dialogMessageSender = dialogMessageSender;
            _queryService = queryService;
        }

        public async Task<Guid> ExecuteAsync(SendMessageModel model)
        {
            Message message = _mapper.Map<Message>(model.Message);
            message.UserId = model.UsersIds.First();

            Dialog dialog = await _dialogRepository.FindAsync(model.DialogId);
            if (dialog == null)
            {
                Dialog newDialog = new Dialog(model.DialogId, model.UsersIds);
                newDialog.AddMessage(message);
                await _dialogRepository.CreateAsync(newDialog);
                await SendDialogToClient(newDialog, model.UsersIds);
            }
            else
            {
                dialog.AddMessage(message);
                await _dialogRepository.UpdateAsync(dialog);
                await SendMessageToClient(message, model.UsersIds.Skip(1));
            }

            return message.DialogId;
        }

        private async Task SendMessageToClient(Message message, IEnumerable<Guid> usersIds)
        {
            var clientMessage = _mapper.Map<MessageClientViewModel>(message);
            foreach (Guid userId in usersIds)
            {
                await _dialogMessageSender.SendMessage(userId.ToString(), clientMessage);
            }
        }

        private async Task SendDialogToClient(Dialog dialog, List<Guid> usersIds)
        {
            var clientModel = new DialogPreviewViewModel()
            {
                Id = dialog.Id,
                LastMessage = _mapper.Map<MessageViewModel>(dialog.Messages.First())
            };

            foreach (Guid userId in usersIds)
            {
                var id = usersIds.First(i => i.ToString() != userId.ToString());
                User userInfo = await _queryService.Users
                    .Include(u => u.Status)
                    .FirstAsync(u => u.Id == id);

                clientModel.User = _mapper.Map<UserShortViewModel>(userInfo);
                
                await _dialogMessageSender.SendDialog(userId.ToString(), clientModel);
            }
        }
    }
}