﻿using System.Threading.Tasks;

namespace Chat.Application.Commands
{
    public interface ICommand<TInput>
    {
        Task ExecuteAsync(TInput model);
    }

    public interface ICommand<TInput, TOutput>
    {
        Task<TOutput> ExecuteAsync(TInput model);
    }
}