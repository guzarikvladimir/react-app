﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Chat.API.Migrations
{
    public partial class UserDialogs : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_UserDialog_Dialogs_DialogId",
                table: "UserDialog");

            migrationBuilder.DropForeignKey(
                name: "FK_UserDialog_Users_UserId",
                table: "UserDialog");

            migrationBuilder.DropPrimaryKey(
                name: "PK_UserDialog",
                table: "UserDialog");

            migrationBuilder.RenameTable(
                name: "UserDialog",
                newName: "UserDialogs");

            migrationBuilder.RenameIndex(
                name: "IX_UserDialog_DialogId",
                table: "UserDialogs",
                newName: "IX_UserDialogs_DialogId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_UserDialogs",
                table: "UserDialogs",
                columns: new[] { "UserId", "DialogId" });

            migrationBuilder.AddForeignKey(
                name: "FK_UserDialogs_Dialogs_DialogId",
                table: "UserDialogs",
                column: "DialogId",
                principalTable: "Dialogs",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_UserDialogs_Users_UserId",
                table: "UserDialogs",
                column: "UserId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_UserDialogs_Dialogs_DialogId",
                table: "UserDialogs");

            migrationBuilder.DropForeignKey(
                name: "FK_UserDialogs_Users_UserId",
                table: "UserDialogs");

            migrationBuilder.DropPrimaryKey(
                name: "PK_UserDialogs",
                table: "UserDialogs");

            migrationBuilder.RenameTable(
                name: "UserDialogs",
                newName: "UserDialog");

            migrationBuilder.RenameIndex(
                name: "IX_UserDialogs_DialogId",
                table: "UserDialog",
                newName: "IX_UserDialog_DialogId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_UserDialog",
                table: "UserDialog",
                columns: new[] { "UserId", "DialogId" });

            migrationBuilder.AddForeignKey(
                name: "FK_UserDialog_Dialogs_DialogId",
                table: "UserDialog",
                column: "DialogId",
                principalTable: "Dialogs",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_UserDialog_Users_UserId",
                table: "UserDialog",
                column: "UserId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
