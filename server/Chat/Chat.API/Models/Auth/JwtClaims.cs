﻿namespace Chat.API.Models.Auth
{
    public static class Jwt
    {
        public static class JwtClaimIdentifiers
        {
            public const string Rol = "rol";
        }

        public static class JwtClaims
        {
            public const string ApiAccess = "api_access";
        }
    }
}