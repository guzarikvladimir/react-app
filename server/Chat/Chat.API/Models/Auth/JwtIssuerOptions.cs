﻿namespace Chat.API.Models.Auth
{
    public class JwtIssuerOptions
    {
        public string Issuer { get; set; }

        public string Audience { get; set; }
    }
}