﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Chat.API.Models
{
    public class UserStatusPresentationModel
    {
        [Required]
        public Guid UserId { get; set; }

        [Required]
        public DateTime LastSeen { get; set; }
    }
}