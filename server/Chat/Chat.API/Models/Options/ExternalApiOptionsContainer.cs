﻿namespace Chat.API.Models.Options
{
    public class ExternalApiOptionsContainer
    {
        public ExternalApiOptions Client { get; set; }
    }
}