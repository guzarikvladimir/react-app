﻿namespace Chat.API.Models.Options
{
    public class ExternalApiOptions
    {
        public string Uri { get; private set; }
    }
}