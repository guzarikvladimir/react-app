﻿using Chat.Application.Commands.CreateUser;
using Chat.Application.Commands.SendMessage;
using Chat.Application.Commands.UpdateUser;
using Chat.Application.Commands.UpdateUserStatus;
using Microsoft.Extensions.DependencyInjection;

namespace Chat.API.DependencyInjection
{
    public static class CommandsExtensions
    {
        public static void AddCommands(this IServiceCollection services)
        {
            services.AddScoped<ISendMessageCommand, SendMessageCommand>();
            services.AddScoped<IUpdateUserStatusCommand, UpdateUserStatusCommand>();
            services.AddScoped<ICreateUserCommand, CreateUserCommand>();
            services.AddScoped<IUpdateUserCommand, UpdateUserCommand>();
        }
    }
}