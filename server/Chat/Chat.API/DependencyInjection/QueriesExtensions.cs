﻿using Chat.Application.Queries.GetContacts;
using Chat.Application.Queries.GetDialog;
using Chat.Application.Queries.GetUserDialogs;
using Chat.Application.Queries.GetUserInfo;
using Microsoft.Extensions.DependencyInjection;

namespace Chat.API.DependencyInjection
{
    public static class QueriesExtensions
    {
        public static void AddQueris(this IServiceCollection services)
        {
            services.AddScoped<IGetUserDialogsQuery, GetUserDialogsQuery>();
            services.AddScoped<IGetUsersInfoQuery, GetUserInfoQuery>();
            services.AddScoped<IGetUserInfoQuery, GetUserInfoQuery>();
            services.AddScoped<IGetDialogQuery, GetDialogQuery>();
            services.AddScoped<IGetContactsQuery, GetContactsQuery>();
        }
    }
}