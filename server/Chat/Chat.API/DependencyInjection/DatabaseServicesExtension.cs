﻿using System.Reflection;
using Chat.Domain.Repositories;
using Chat.Infrastructure.EF;
using Chat.Infrastructure.EF.Repositories;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace Chat.API.DependencyInjection
{
    public static class DatabaseServicesExtension
    {
        public static void AddRepositories(this IServiceCollection services, string connectionString)
        {
            services.AddDbContext<ChatDbContext>(options =>
                options.UseSqlServer(connectionString, sqlOptions =>
                    {
                        sqlOptions.MigrationsAssembly(typeof(Startup).GetTypeInfo().Assembly.GetName().Name);
                    }));

            services.AddScoped<IDialogRepository, DialogRepository>();
            services.AddScoped<IUserStatusRepository, UserStatusRepository>();
            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<IQueryService, DatabaseQueryService>();
        }
    }
}