﻿using Chat.Infrastructure.SignalR.Senders;
using Microsoft.Extensions.DependencyInjection;

namespace Chat.API.DependencyInjection
{
    public static class SendersExtensions
    {
        public static void AddRealTimeSenders(this IServiceCollection services)
        {
            services.AddSignalR();
            services.AddHostedService<DialogStatusSender>();
        }
    }
}