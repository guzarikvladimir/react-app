using AutoMapper;
using Chat.API.DependencyInjection;
using Chat.API.Models.Auth;
using Chat.API.Models.Options;
using Chat.Application;
using Chat.Domain.Clients.Services;
using Chat.Infrastructure.SignalR.Hubs;
using Chat.Infrastructure.SignalR.Senders;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace Chat.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddRepositories(Configuration.GetConnectionString("DefaultConnection"));

            services.AddControllers();

            var jwtOptions = Configuration
                .GetSection(nameof(JwtIssuerOptions))
                .Get<JwtIssuerOptions>(x => x.BindNonPublicProperties = true);
            services.AddAuth(jwtOptions);

            ExternalApiOptionsContainer externalApis = Configuration
                .GetSection("ExternalApis")
                .Get<ExternalApiOptionsContainer>(x => x.BindNonPublicProperties = true);
            services.AddCors(setup =>
            {
                setup.AddDefaultPolicy(policy =>
                {
                    policy.AllowAnyHeader();
                    policy.AllowAnyMethod();
                    policy.WithOrigins(externalApis.Client.Uri);
                    policy.AllowCredentials();
                });
            });

            services.AddTransient<IDialogMessageSender, DialogMessageSender>();

            services.AddRealTimeSenders();
            services.AddQueris();
            services.AddCommands();
            services.AddAutoMapper(typeof(Startup), typeof(ApplicationModule));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseCors();
            app.UseRouting();
            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapHub<ChatHub>("/chatHub");
                endpoints.MapControllers();
            });
        }
    }
}
