﻿using System.Threading.Tasks;
using AutoMapper;
using Chat.API.Models;
using Chat.API.Models.Auth;
using Chat.Application.Commands.UpdateUserStatus;
using Chat.Application.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Chat.API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    [Authorize(Policy = Policy.ApiUser)]
    public class UserStatusController : Controller
    {
        private readonly IUpdateUserStatusCommand _updateUserStatusCommand;
        private readonly IMapper _mapper;

        public UserStatusController(
            IUpdateUserStatusCommand updateUserStatusCommand,
            IMapper mapper)
        {
            _updateUserStatusCommand = updateUserStatusCommand;
            _mapper = mapper;
        }

        [HttpPost]
        [Route("create")]
        [AllowAnonymous]
        public async Task<IActionResult> Create([FromBody] UserStatusPresentationModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            await _updateUserStatusCommand.ExecuteAsync(_mapper.Map<UserStatusModel>(model));

            return Ok();
        }

        [HttpPost]
        public async Task<IActionResult> Update([FromBody] UserStatusPresentationModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            await _updateUserStatusCommand.ExecuteAsync(_mapper.Map<UserStatusModel>(model));

            return Ok();
        }
    }
}