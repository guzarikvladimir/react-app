﻿using System;
using System.Threading.Tasks;
using Chat.API.Models.Auth;
using Chat.Application.Commands.SendMessage;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Chat.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Policy = Policy.ApiUser)]
    public class MessagesController : Controller
    {
        private readonly ISendMessageCommand _sendMessageCommand;

        public MessagesController(ISendMessageCommand sendMessageCommand)
        {
            _sendMessageCommand = sendMessageCommand;
        }

        [HttpPost]
        public async Task<Guid> SendMessage([FromBody] SendMessageModel model)
        {
            return await _sendMessageCommand.ExecuteAsync(model);
        }
    }
}