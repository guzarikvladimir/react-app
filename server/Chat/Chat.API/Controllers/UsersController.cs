﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Chat.API.Models.Auth;
using Chat.Application.Commands.CreateUser;
using Chat.Application.Commands.UpdateUser;
using Chat.Application.Models;
using Chat.Application.Queries.GetContacts;
using Chat.Application.Queries.GetUserInfo;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Chat.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Policy = Policy.ApiUser)]
    public class UsersController : Controller
    {
        private readonly IGetUserInfoQuery _getUserInfoQuery;
        private readonly IGetUsersInfoQuery _getUsersInfoQuery;
        private readonly ICreateUserCommand _createUserCommand;
        private readonly IGetContactsQuery _getContactsQuery;
        private readonly IUpdateUserCommand _updateUserCommand;

        public UsersController(
            IGetUserInfoQuery getUserInfoQuery,
            IGetUsersInfoQuery getUsersInfoQuery,
            ICreateUserCommand createUserCommand,
            IGetContactsQuery getContactsQuery,
            IUpdateUserCommand updateUserCommand)
        {
            _getUserInfoQuery = getUserInfoQuery;
            _getUsersInfoQuery = getUsersInfoQuery;
            _createUserCommand = createUserCommand;
            _getContactsQuery = getContactsQuery;
            _updateUserCommand = updateUserCommand;
        }

        [HttpPost]
        [Route("info")]
        public async Task<IEnumerable<UserViewModel>> GetUsersInfo([FromBody] List<Guid> usersIds)
        {
            return await _getUsersInfoQuery.ExecuteAsync(usersIds);
        }

        [HttpPost]
        public async Task<IEnumerable<UserViewModel>> GetContacts([FromBody] GetContactsModel model)
        {
            return await _getContactsQuery.ExecuteAsync(model);
        }

        [HttpGet]
        [Route("info/{userId}")]
        public async Task<UserViewModel> GetUser(Guid userId)
        {
            return await _getUserInfoQuery.ExecuteAsync(userId);
        }

        [HttpPost]
        [Route("create")]
        [AllowAnonymous]
        public async Task<IActionResult> Create([FromBody] UserModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            await _createUserCommand.ExecuteAsync(model);

            return Ok("Profile created");
        }

        [HttpPost]
        [Route("update")]
        public async Task Update([FromBody] UserModel model)
        {
            await _updateUserCommand.ExecuteAsync(model);
        }
    }
}