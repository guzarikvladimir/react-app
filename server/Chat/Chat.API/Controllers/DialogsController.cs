﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Chat.API.Models.Auth;
using Chat.Application.Models;
using Chat.Application.Queries.GetDialog;
using Chat.Application.Queries.GetUserDialogs;
using Chat.Domain.Clients.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Chat.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Policy = Policy.ApiUser)]
    public class DialogsController : Controller
    {
        private readonly IGetUserDialogsQuery _getUserDialogsQuery;
        private readonly IGetDialogQuery _getDialogQuery;

        public DialogsController(
            IGetUserDialogsQuery getUserDialogsQuery,
            IGetDialogQuery getDialogQuery)
        {
            _getUserDialogsQuery = getUserDialogsQuery;
            _getDialogQuery = getDialogQuery;
        }

        [HttpGet]
        [Route("{dialogId}")]
        public async Task<DialogViewModel> GetDialog(Guid dialogId)
        {
            return await _getDialogQuery.ExecuteAsync(dialogId);
        }

        [HttpGet]
        [Route("user/{userId}")]
        public async Task<IEnumerable<DialogPreviewViewModel>> GetUserDialogs(Guid userId)
        {
            return await _getUserDialogsQuery.ExecuteAsync(userId);
        }
    }
}