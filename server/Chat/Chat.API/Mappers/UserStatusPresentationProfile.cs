﻿using AutoMapper;
using Chat.API.Models;
using Chat.Application.Models;

namespace Chat.API.Mappers
{
    public class UserStatusPresentationProfile : Profile
    {
        public UserStatusPresentationProfile()
        {
            CreateMap<UserStatusPresentationModel, UserStatusModel>();
        }
    }
}