﻿using System;

namespace Chat.Domain.Models
{
    public class UserDialog
    {
        public Guid UserId { get; set; }
        public User User { get; }

        public Guid DialogId { get; set; }
        public Dialog Dialog { get; }

        public UserDialog(Guid dialogId, Guid userId)
        {
            DialogId = dialogId;
            UserId = userId;
        }
    }
}