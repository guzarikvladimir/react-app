﻿namespace Chat.Domain.Models
{
    public enum UserStatusType
    {
        Offline,
        Online
    }
}