﻿using System;
using System.Collections.Generic;

namespace Chat.Domain.Models
{
    public class User
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public string Email { get; set; }

        public string? PhoneNumber { get; set; }

        public string? ProfileImg { get; set; }

        private readonly List<UserDialog> _userDialogs;
        public IReadOnlyCollection<UserDialog> UserDialogs => _userDialogs;

        public List<Message> Messages { get; }

        public UserStatus Status { get; }

        public User()
        {
            Status = new UserStatus(Id);
            _userDialogs = new List<UserDialog>();
        }
    }
}