﻿using System;

namespace Chat.Domain.Models
{
    public class Message
    {
        public Guid Id { get; set; }

        public string Text { get; set; }

        public DateTime CreatedDate { get; set; }

        public Guid DialogId { get; set; }
        public Dialog Dialog { get; }

        public Guid UserId { get; set; }
        public User User { get; }

        public Message(string text, DateTime createdDate)
        {
            Text = text;
            CreatedDate = createdDate;
        }
    }
}