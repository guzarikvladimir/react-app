﻿using System;

namespace Chat.Domain.Models
{
    public class UserStatus
    {
        public Guid UserId { get; set; }

        public DateTime LastSeen { get; set; }

        public UserStatusType Status { get; set; }

        public User User { get; }

        public UserStatus(Guid userId)
        {
            UserId = userId;
            LastSeen = DateTime.UtcNow;
            Status = UserStatusType.Offline;
        }
    }
}