﻿using System;
using System.Collections.Generic;

namespace Chat.Domain.Models
{
    public class Dialog
    {
        public Guid Id { get; set; }

        //public string GroupName { get; set; }

        private readonly List<Message> _messages;
        public IReadOnlyCollection<Message> Messages => _messages;

        private readonly List<UserDialog> _userDialogs;
        public IReadOnlyCollection<UserDialog> UserDialogs => _userDialogs;

        public Dialog()
        {
            _userDialogs = new List<UserDialog>();
            _messages = new List<Message>();
        }

        public Dialog(Guid dialogId, List<Guid> usersIds) : this()
        {
            Id = dialogId;
            foreach (Guid userId in usersIds)
            {
                _userDialogs.Add(new UserDialog(Id, userId));
            }
        }

        public void AddMessage(Message message)
        {
            message.DialogId = Id;
            _messages.Add(message);
        }
    }
}