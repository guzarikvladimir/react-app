﻿using System;

namespace Chat.Domain.Clients.Models
{
    public class DialogPreviewViewModel
    {
        public Guid Id { get; set; }

        public UserShortViewModel User { get; set; }

        public MessageViewModel LastMessage { get; set; }
    }
}