﻿using System;

namespace Chat.Domain.Clients.Models
{
    public class MessageClientViewModel
    {
        public string Text { get; set; }

        public DateTime CreatedDate { get; set; }

        public Guid UserId { get; set; }

        public Guid DialogId { get; set; }
    }
}