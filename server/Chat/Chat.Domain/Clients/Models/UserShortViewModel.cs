﻿namespace Chat.Domain.Clients.Models
{
    public class UserShortViewModel
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public string? ProfileImg { get; set; }

        public UserStatusViewModel Status { get; set; }
    }
}