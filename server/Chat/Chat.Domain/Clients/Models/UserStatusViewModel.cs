﻿using System;
using Chat.Domain.Models;

namespace Chat.Domain.Clients.Models
{
    public class UserStatusViewModel
    {
        public DateTime LastSeen { get; set; }

        public UserStatusType Status { get; set; }
    }
}