﻿using System.Threading.Tasks;
using Chat.Domain.Clients.Models;

namespace Chat.Domain.Clients.Services
{
    public interface IDialogMessageSender
    {
        Task SendMessage(string userId, MessageClientViewModel message);

        Task SendDialog(string userId, DialogPreviewViewModel dialog);
    }
}