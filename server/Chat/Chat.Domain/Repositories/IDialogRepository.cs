﻿using Chat.Domain.Models;

namespace Chat.Domain.Repositories
{
    public interface IDialogRepository : IRepository<Dialog>
    {
    }
}