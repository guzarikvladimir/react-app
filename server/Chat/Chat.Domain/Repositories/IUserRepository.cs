﻿using Chat.Domain.Models;

namespace Chat.Domain.Repositories
{
    public interface IUserRepository : IRepository<User>
    {
    }
}