﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Chat.Domain.Specifications;

namespace Chat.Domain.Repositories
{
    public interface IRepository<TEntity>
    {
        Task<IEnumerable<TEntity>> FindAllAsync(ISpecification<TEntity> spec);

        Task<TEntity> FindAsync(ISpecification<TEntity> spec);

        Task<TEntity> FindAsync(Guid id);

        Task UpdateAsync(TEntity item);

        Task CreateAsync(TEntity item);

        Task RemoveAsync(TEntity item);

        Task SaveChangesAsync();
    }
}