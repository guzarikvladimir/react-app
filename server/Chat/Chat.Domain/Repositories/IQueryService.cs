﻿using System.Linq;
using Chat.Domain.Models;

namespace Chat.Domain.Repositories
{
    public interface IQueryService
    {
        IQueryable<Dialog> Dialogs { get; }

        IQueryable<Message> Messages { get; }

        IQueryable<UserStatus> UserStatuses { get; }

        IQueryable<User> Users { get; }

        IQueryable<UserDialog> UsersDialogs { get; }
    }
}