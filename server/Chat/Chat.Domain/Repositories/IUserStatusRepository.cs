﻿using Chat.Domain.Models;

namespace Chat.Domain.Repositories
{
    public interface IUserStatusRepository : IRepository<UserStatus>
    {
    }
}