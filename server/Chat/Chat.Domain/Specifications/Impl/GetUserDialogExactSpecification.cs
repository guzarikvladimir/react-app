﻿using System;
using System.Collections.Generic;
using System.Linq;
using Chat.Domain.Models;

namespace Chat.Domain.Specifications.Impl
{
    public class GetUserDialogExactSpecification : Specification<Dialog>
    {
        public GetUserDialogExactSpecification(List<Guid> usersIds)
            : base(d => d.UserDialogs.Any(ud => usersIds.Contains(ud.UserId)))
        {
            Includes.Add(d => d.UserDialogs);
            Includes.Add(d => d.Messages);
        }
    }
}