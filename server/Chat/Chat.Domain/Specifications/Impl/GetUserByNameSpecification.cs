﻿using Chat.Domain.Models;

namespace Chat.Domain.Specifications.Impl
{
    public class GetUserByNameSpecification : Specification<User>
    {
        public GetUserByNameSpecification(string userName)
            : base(u => u.Email == userName)
        {
        }
    }
}