﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Chat.Domain.Specifications.Impl
{
    public class Specification<T> : ISpecification<T>
    {
        public Expression<Func<T, bool>> Criteria { get; }

        public List<Expression<Func<T, object>>> Includes { get; }

        public List<string> IncludeStrings { get; }

        public Specification(Expression<Func<T, bool>> criteria)
        {
            Criteria = criteria;
            Includes = new List<Expression<Func<T, object>>>();
            IncludeStrings = new List<string>();
        }

        public Specification(Expression<Func<T, bool>> criteria, List<Expression<Func<T, object>>> includes)
            : this(criteria)
        {
            Includes.AddRange(includes);
        }

        public Specification(Expression<Func<T, bool>> criteria, List<string> includeStrings)
            : this(criteria)
        {
            IncludeStrings.AddRange(includeStrings);
        }
    }
}