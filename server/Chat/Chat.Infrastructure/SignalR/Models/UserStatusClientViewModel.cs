﻿using System;

namespace Chat.Infrastructure.SignalR.Models
{
    public class UserStatusClientViewModel
    {
        public Guid UserId { get; set; }

        public DateTime? LastSeen { get; set; }
    }
}