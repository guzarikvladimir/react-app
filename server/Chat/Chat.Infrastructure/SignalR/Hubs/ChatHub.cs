﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Chat.Domain.Models;
using Chat.Domain.Repositories;
using Chat.Infrastructure.SignalR.Clients;
using Microsoft.AspNetCore.SignalR;

namespace Chat.Infrastructure.SignalR.Hubs
{
    public class ChatHub : Hub<IChatClient>
    {
        private readonly IUserStatusRepository _userStatusRepository;
        
        public static Dictionary<string, Guid> Users { get; private set; } = new Dictionary<string, Guid>();

        public ChatHub(IUserStatusRepository userStatusRepository)
        {
            _userStatusRepository = userStatusRepository;
        }

        public override async Task OnConnectedAsync()
        {
            string userId = Context.GetHttpContext().Request.Query["userId"].FirstOrDefault() ?? string.Empty;
            UserStatus userStatus = await _userStatusRepository.FindAsync(new Guid(userId));
            if (userStatus == null)
            {
                throw new Exception("Invalid client");
            }

            await UpdateStatus(userStatus, UserStatusType.Online);

            Users.Add(Context.ConnectionId, new Guid(userId));

            await Groups.AddToGroupAsync(Context.ConnectionId, userId);

            await base.OnConnectedAsync();
        }

        public override async Task OnDisconnectedAsync(Exception exception)
        {
            if (Users.TryGetValue(Context.ConnectionId, out Guid userId))
            {
                UserStatus userStatus = await _userStatusRepository.FindAsync(userId);
                await UpdateStatus(userStatus, UserStatusType.Offline);

                Users.Remove(Context.ConnectionId);
            }

            await base.OnDisconnectedAsync(exception);
        }

        public async Task UpdateStatus(UserStatus userStatus, UserStatusType status)
        {
            userStatus.LastSeen = DateTime.UtcNow;
            userStatus.Status = status;
            await _userStatusRepository.UpdateAsync(userStatus);
        }
    }
}