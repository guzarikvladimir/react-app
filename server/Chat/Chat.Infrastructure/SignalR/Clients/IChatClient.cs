﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Chat.Domain.Clients.Models;
using Chat.Infrastructure.SignalR.Models;

namespace Chat.Infrastructure.SignalR.Clients
{
    public interface IChatClient
    {
        Task ReceiveMessage(MessageClientViewModel message);

        Task ReceiveDialog(DialogPreviewViewModel dialog);

        Task UpdateStatuses(List<UserStatusClientViewModel> usersStatuses);
    }
}