﻿using System.Threading.Tasks;
using Chat.Domain.Clients.Models;
using Chat.Domain.Clients.Services;
using Chat.Infrastructure.SignalR.Clients;
using Chat.Infrastructure.SignalR.Hubs;
using Microsoft.AspNetCore.SignalR;

namespace Chat.Infrastructure.SignalR.Senders
{
    public class DialogMessageSender : IDialogMessageSender
    {
        private readonly IHubContext<ChatHub, IChatClient> _chatHubContext;

        public DialogMessageSender(IHubContext<ChatHub, IChatClient> chatHubContext)
        {
            _chatHubContext = chatHubContext;
        }

        public async Task SendMessage(string userId, MessageClientViewModel message)
        {
            await _chatHubContext.Clients.Group(userId).ReceiveMessage(message);
        }

        public async Task SendDialog(string userId, DialogPreviewViewModel dialog)
        {
            await _chatHubContext.Clients.Group(userId).ReceiveDialog(dialog);
        }
    }
}