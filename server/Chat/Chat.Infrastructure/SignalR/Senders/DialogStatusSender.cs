﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Chat.Domain.Models;
using Chat.Domain.Repositories;
using Chat.Infrastructure.SignalR.Clients;
using Chat.Infrastructure.SignalR.Hubs;
using Chat.Infrastructure.SignalR.Models;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace Chat.Infrastructure.SignalR.Senders
{
    public class DialogStatusSender : BackgroundService
    {
        private readonly IHubContext<ChatHub, IChatClient> _chatHubContext;
        private readonly IServiceProvider _serviceProvider;

        public DialogStatusSender(
            IHubContext<ChatHub, IChatClient> chatHubContext,
            IServiceProvider serviceProvider)
        {
            _chatHubContext = chatHubContext;
            _serviceProvider = serviceProvider;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            using (var scope = _serviceProvider.CreateScope())
            {
                IQueryService queryService = scope.ServiceProvider.GetService<IQueryService>();

                while (!stoppingToken.IsCancellationRequested)
                {
                    List<UserStatusClientViewModel> usersStatuses = new List<UserStatusClientViewModel>();
                    Guid[] activeUsers = ChatHub.Users.Values.Distinct().ToArray();
                    foreach (Guid targetUser in activeUsers)
                    {
                        Guid[] userDialogs = await queryService.UsersDialogs
                            .Where(ud => ud.UserId == targetUser)
                            .Select(ud => ud.DialogId)
                            .ToArrayAsync(stoppingToken);

                        Guid[] users = await queryService.UsersDialogs
                            .Where(ud => userDialogs.Contains(ud.DialogId) && ud.UserId != targetUser)
                            .Select(ud => ud.UserId)
                            .Distinct()
                            .ToArrayAsync(stoppingToken);

                        foreach (Guid user in users)
                        {
                            UserStatusClientViewModel userStatus = new UserStatusClientViewModel();
                            userStatus.UserId = user;
                            if (!activeUsers.Contains(user))
                            {
                                UserStatus status = await queryService.UserStatuses
                                    .FirstAsync(us => us.UserId == user, stoppingToken);
                                userStatus.LastSeen = status.LastSeen;
                            }

                            usersStatuses.Add(userStatus);
                        }

                        await _chatHubContext.Clients.Group(targetUser.ToString()).UpdateStatuses(usersStatuses);
                    }

                    await Task.Delay(TimeSpan.FromSeconds(20), stoppingToken);
                }
            }
        }
    }
}