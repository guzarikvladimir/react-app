﻿using Chat.Domain.Models;
using Chat.Infrastructure.EF.Configurations;
using Microsoft.EntityFrameworkCore;

namespace Chat.Infrastructure.EF
{
    public class ChatDbContext : DbContext
    {
        public DbSet<Message> Messages { get; set; }

        public DbSet<Dialog> Dialogs { get; set; }

        public DbSet<UserStatus> UserStatuses { get; set; }

        public DbSet<User> Users { get; set; }

        public DbSet<UserDialog> UserDialogs { get; set; }

        public ChatDbContext(DbContextOptions<ChatDbContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.UseIdentityColumns();

            modelBuilder.ApplyConfiguration(new DialogTypeConfiguration());
            modelBuilder.ApplyConfiguration(new MessageTypeConfiguration());
            modelBuilder.ApplyConfiguration(new UserStatusTypeConfiguration());
            modelBuilder.ApplyConfiguration(new UserTypeConfiguration());
            modelBuilder.ApplyConfiguration(new UserDialogTypeConfiguration());
        }
    }
}