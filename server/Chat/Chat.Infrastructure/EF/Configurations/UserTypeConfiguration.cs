﻿using Chat.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Chat.Infrastructure.EF.Configurations
{
    public class UserTypeConfiguration : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.HasKey(u => u.Id);

            builder.Property(u => u.Name).IsRequired();

            builder.Property(u => u.Email).IsRequired();

            builder.Property(u => u.PhoneNumber);

            builder.Property(u => u.ProfileImg);

            builder.HasMany(u => u.UserDialogs)
                .WithOne(ud => ud.User)
                .HasForeignKey(u => u.UserId);

            builder.Metadata
                .FindNavigation(nameof(User.UserDialogs))
                .SetPropertyAccessMode(PropertyAccessMode.Field);
        }
    }
}