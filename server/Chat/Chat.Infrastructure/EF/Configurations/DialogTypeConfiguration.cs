﻿using Chat.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Chat.Infrastructure.EF.Configurations
{
    public class DialogTypeConfiguration : IEntityTypeConfiguration<Dialog>
    {
        public void Configure(EntityTypeBuilder<Dialog> builder)
        {
            builder.HasKey(d => d.Id);

            builder.HasMany(u => u.Messages)
                .WithOne(m => m.Dialog)
                .HasForeignKey(u => u.DialogId);

            builder.HasMany(u => u.UserDialogs)
                .WithOne(ud => ud.Dialog)
                .HasForeignKey(u => u.DialogId);

            builder.Metadata
                .FindNavigation(nameof(Dialog.Messages))
                .SetPropertyAccessMode(PropertyAccessMode.Field);

            builder.Metadata
                .FindNavigation(nameof(Dialog.UserDialogs))
                .SetPropertyAccessMode(PropertyAccessMode.Field);
        }
    }
}