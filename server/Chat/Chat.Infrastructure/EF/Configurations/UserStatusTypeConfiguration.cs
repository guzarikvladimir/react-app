﻿using Chat.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Chat.Infrastructure.EF.Configurations
{
    public class UserStatusTypeConfiguration : IEntityTypeConfiguration<UserStatus>
    {
        public void Configure(EntityTypeBuilder<UserStatus> builder)
        {
            builder.HasKey(u => u.UserId);

            builder.Property(u => u.LastSeen);

            builder.Property(u => u.Status).HasDefaultValue(UserStatusType.Offline);

            builder.HasOne(us => us.User)
                .WithOne(u => u.Status)
                .HasForeignKey<UserStatus>(us => us.UserId);
        }
    }
}