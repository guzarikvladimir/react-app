﻿using Chat.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Chat.Infrastructure.EF.Configurations
{
    public class UserDialogTypeConfiguration : IEntityTypeConfiguration<UserDialog>
    {
        public void Configure(EntityTypeBuilder<UserDialog> builder)
        {
            builder.HasKey(ud => new {ud.UserId, ud.DialogId});

            builder.HasOne(ud => ud.User)
                .WithMany(u => u.UserDialogs)
                .HasForeignKey(ud => ud.UserId);

            builder.HasOne(ud => ud.Dialog)
                .WithMany(d => d.UserDialogs)
                .HasForeignKey(ud => ud.DialogId);

            builder.Metadata.FindNavigation(nameof(UserDialog.Dialog))
                .SetPropertyAccessMode(PropertyAccessMode.Field);

            builder.Metadata.FindNavigation(nameof(UserDialog.User))
                .SetPropertyAccessMode(PropertyAccessMode.Field);
        }
    }
}