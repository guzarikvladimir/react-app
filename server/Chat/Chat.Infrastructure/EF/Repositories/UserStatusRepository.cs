﻿using Chat.Domain.Models;
using Chat.Domain.Repositories;

namespace Chat.Infrastructure.EF.Repositories
{
    public class UserStatusRepository : EfRepository<UserStatus>, IUserStatusRepository
    {
        public UserStatusRepository(ChatDbContext dataContext) : base(dataContext)
        {
        }
    }
}