﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Chat.Domain.Repositories;
using Chat.Domain.Specifications;
using Microsoft.EntityFrameworkCore;

namespace Chat.Infrastructure.EF.Repositories
{
    public class EfRepository<T> : IRepository<T>
        where T : class
    {
        protected readonly ChatDbContext Context;
        protected readonly DbSet<T> DbSet;

        public EfRepository(ChatDbContext dataContext)
        {
            Context = dataContext;
            DbSet = dataContext.Set<T>();
        }

        public async Task<IEnumerable<T>> FindAllAsync(ISpecification<T> spec)
        {
            IQueryable<T> query = BuildQueryBySpec(spec);
            return await query.ToListAsync();
        }

        public async Task<T> FindAsync(ISpecification<T> spec)
        {
            IQueryable<T> query = BuildQueryBySpec(spec);
            return await query.FirstOrDefaultAsync();
        }

        public async Task<T> FindAsync(Guid id)
        {
            return await DbSet.FindAsync(id);
        }

        public async Task UpdateAsync(T item)
        {
            Context.Update(item);
            await Context.SaveChangesAsync();
        }

        public async Task SaveChangesAsync()
        {
            await Context.SaveChangesAsync();
        }

        public async Task CreateAsync(T item)
        {
            Context.Add(item);
            await Context.SaveChangesAsync();
        }

        public async Task RemoveAsync(T item)
        {
            DbSet.Remove(item);
            await Context.SaveChangesAsync();
        }

        protected IQueryable<T> BuildQueryBySpec(ISpecification<T> spec)
        {
            IQueryable<T> query = spec
                .Includes
                .Aggregate(DbSet.AsQueryable(), (current, include) => current.Include(include));

            query = spec
                .IncludeStrings
                .Aggregate(query, (current, include) => current.Include(include));

            if (spec.Criteria != null)
            {
                query = query.Where(spec.Criteria);
            }

            return query;
        }
    }
}