﻿using Chat.Domain.Models;
using Chat.Domain.Repositories;

namespace Chat.Infrastructure.EF.Repositories
{
    public class UserRepository : EfRepository<User>, IUserRepository
    {
        public UserRepository(ChatDbContext dataContext) : base(dataContext)
        {
        }
    }
}