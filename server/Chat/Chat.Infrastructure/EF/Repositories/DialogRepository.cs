﻿using Chat.Domain.Models;
using Chat.Domain.Repositories;

namespace Chat.Infrastructure.EF.Repositories
{
    public class DialogRepository : EfRepository<Dialog>, IDialogRepository
    {
        public DialogRepository(ChatDbContext dataContext) : base(dataContext)
        {
        }
    }
}