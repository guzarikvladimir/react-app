﻿using System.Linq;
using Chat.Domain.Models;
using Chat.Domain.Repositories;
using Microsoft.EntityFrameworkCore;

namespace Chat.Infrastructure.EF
{
    public class DatabaseQueryService : IQueryService
    {
        private readonly ChatDbContext _context;

        public DatabaseQueryService(ChatDbContext context)
        {
            _context = context;
        }

        public IQueryable<Dialog> Dialogs => _context.Dialogs.AsNoTracking();

        public IQueryable<Message> Messages => _context.Messages.AsNoTracking();

        public IQueryable<UserStatus> UserStatuses => _context.UserStatuses.AsNoTracking();

        public IQueryable<User> Users => _context.Users.AsNoTracking();

        public IQueryable<UserDialog> UsersDialogs => _context.UserDialogs.AsNoTracking();
    }
}